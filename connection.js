const Sequelize = require("sequelize");

//dbname,user,password,host
const localConnection = new Sequelize("kapra_bazar", "root", "", {
  host: "localhost",
  dialect: "mysql",
});

//server connection
// const sequelize = new Sequelize(
//   "heroku_f0106001a89edd5",
//   "bfeaafb8a11fa5",
//   "e90c830c",
//   {
//     host: "us-cdbr-east-05.cleardb.net",
//     dialect: "mysql",
//   }
// );

localConnection
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
  });

module.exports = localConnection;
