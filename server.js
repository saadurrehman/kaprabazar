const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const passport = require("passport");
const cookieSession = require("cookie-session");
const connection = require("./connection");
const Models = require("./models")(connection);
const path = require("path");

require("dotenv").config();
require("./authService/passport-google-setup")(Models, passport);
require("./authService/passport-facebook-setup")(Models, passport);
require("./authService/passport-local-setup")(Models, passport);

const PORT = process.env.PORT || 5000;
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//to ready json body
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false, //parse with querystring library
  })
);

//define cors
app.use(cors());

// For an actual app you should configure this with an experation time, better keys, proxy and secure
app.use(
  cookieSession({
    name: "tuto-session",
    keys: ["key1", "key2"],
    maxAge: 24 * 50 * 60 * 1000, // 24hour
    resave: true,
    saveUninitialized: true,
  })
);

// Initializes passport and passport sessions
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  console.log("Request Type:", req.url);
  console.log("Time:", Date.now());
  next();
});

// Google Auth Routes
app.get(
  "/google",
  passport.authenticate("google", {
    scope: ["profile", "email"],
    accessType: "offline",
    prompt: "consent",
  })
);

app.get(
  "/google/callback",
  passport.authenticate("google", {
    failureRedirect: "/api/user/getuser",
    successRedirect: process.env.CLIENT_URL,
  })
);

// Facebook Auth Routes
app.get("/facebook", passport.authenticate("facebook", { scope: "email" }));

app.get(
  "/facebook/callback",
  passport.authenticate("facebook", {
    failureRedirect: "/api/user/getuser",
    successRedirect: process.env.CLIENT_URL,
  })
);

//routes
app.use("/api/user", require("./routes/User"));
app.use("/api/product/category", require("./routes/Category"));
app.use("/api/products", require("./routes/Products"));
app.use("/api/checkout", require("./routes/Stripe"));
app.use("/api/order", require("./routes/Orderdetail"));

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "client", "build")));
  app.get("*", (req, resp) => {
    resp.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
}

app.listen(PORT, () => console.log(`connected with the port: ${PORT}`));
