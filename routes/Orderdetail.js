const express = require("express");
const router = express.Router();
const connection = require("../connection");
const Models = require("../models")(connection);
const { Orderdetail: OrderController } = require("../controllers/index");

router.get("/:userID", OrderController.getOrders(Models));

module.exports = router;
