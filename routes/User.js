const express = require("express");
const passport = require("passport");
const { isLoggedIn } = require("../authService/middleware/middleware");
const router = express.Router();
const connection = require("../connection");
const Models = require("../models")(connection);
const { User: UserController } = require("../controllers/index");

router.post("/register", UserController.registerUser(Models));
router.get("/register/verify", UserController.verifyUser(Models));
router.post("/register/otp", isLoggedIn, UserController.RegisterOTP(Models));
router.post("/verify/otp", isLoggedIn, UserController.verifyOTP(Models));

router.post(
  "/login",
  passport.authenticate("login", {
    failureRedirect: "/api/user/getuser",
    successRedirect: "/api/user/getuser",
  })
);
router.patch("/", isLoggedIn, UserController.optimisticUpdateUserInfo(Models));

// get user
router.get("/getuser", isLoggedIn, (req, res) => res.json(req.user));

router.get("/logout", (req, res) => {
  req.session = null;
  req.logout();
  res.json({ logout: true });
});

module.exports = router;
