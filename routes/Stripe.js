const express = require("express");
const router = express.Router();
const connection = require("../connection");
const Models = require("../models")(connection);
const { Stripe: StripeController } = require("../controllers/index");

router.post("/", StripeController.checkoutWithCashOnDelivery(Models));
router.post("/stripe/pay", StripeController.checkoutWithPaymentMethod(Models));

module.exports = router;
