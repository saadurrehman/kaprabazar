const express = require("express");
const router = express.Router();
const connection = require("../connection");
const Models = require("../models")(connection);
const { Products: ProductsController } = require("../controllers/index");

router.get("/", ProductsController.getAllProducts(Models));
router.get(
  "/:categoryID/:subCategoryID",
  ProductsController.getProductsByCategoryID(Models)
);

module.exports = router;
