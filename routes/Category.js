const express = require("express");
const router = express.Router();
const connection = require("../connection");
const Models = require("../models")(connection);
const { Category: CategoryController } = require("../controllers/index");

router.get("/", CategoryController.getAllCategories(Models));

module.exports = router;
