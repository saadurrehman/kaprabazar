-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2023 at 09:21 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kapra_bazar`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_address` text NOT NULL,
  `order_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `order_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `order_price` int(11) NOT NULL,
  `order_payment_method` varchar(100) NOT NULL,
  `order_delivery_status` varchar(100) NOT NULL,
  `stripe_payment_id` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`order_id`, `user_id`, `order_address`, `order_created_at`, `order_updated_at`, `order_price`, `order_payment_method`, `order_delivery_status`, `stripe_payment_id`) VALUES
(1, 3, 'hello world 123', '2021-01-02 16:00:00', '2021-01-02 16:00:00', 455, 'cash on delivery', 'pending', NULL),
(37, 22, 'fgefsdgsdgsdg', '2021-05-19 17:49:32', '2021-05-19 17:49:32', 4700, 'Cash_on_delivery', 'pending', NULL),
(38, 22, 'fgefsdgsdgsdg', '2021-05-19 17:52:28', '2021-05-19 17:52:28', 10700, 'Cash_on_delivery', 'pending', NULL),
(39, 22, 'fgefsdgsdgsdg', '2021-05-19 17:53:07', '2021-05-19 17:53:07', 10700, 'Card_Payment', 'pending', 'pi_1IstZtDKXc5tKEQCFHO5omGi'),
(40, 35, 'enter123@demo.com', '2023-03-13 19:54:55', '2023-03-13 19:54:55', 3000, 'Cash_on_delivery', 'pending', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_detail`
--

CREATE TABLE `order_product_detail` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_product_detail`
--

INSERT INTO `order_product_detail` (`order_id`, `product_id`, `product_quantity`) VALUES
(37, 1, 1),
(37, 3, 1),
(38, 1, 1),
(38, 3, 1),
(38, 10, 1),
(38, 9, 1),
(39, 1, 1),
(39, 3, 1),
(39, 10, 1),
(39, 9, 1),
(40, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_description` text NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_discountprice` int(11) DEFAULT NULL,
  `product_originalprice` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `product_add_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `product_modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `product_subcategory_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_description`, `product_quantity`, `product_discountprice`, `product_originalprice`, `product_category_id`, `product_add_date`, `product_modified_date`, `product_subcategory_id`) VALUES
(1, 'Blue Jacket', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 6, 0, 2200, 2, '2021-05-01 18:53:55', '2021-05-01 18:53:55', 2),
(2, 'Green jacket', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 10, 0, 2200, 2, '2021-05-01 19:10:50', '2021-05-01 19:10:50', 2),
(3, 'Black jacket', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 6, 0, 2500, 2, '2021-05-01 19:25:05', '2021-05-01 19:25:05', 2),
(4, 'Suit 1', 't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1, 2000, 2500, 1, '2021-05-01 19:29:34', '2021-05-01 19:29:34', 4),
(5, 'Suit 2', 't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 5, 3000, 4000, 1, '2021-05-01 19:29:34', '2021-05-01 19:29:34', 4),
(6, 'Suit 3', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 4, 3500, 4000, 1, '2021-05-01 19:43:25', '2021-05-01 19:43:25', 4),
(7, 'suit 4', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 5, 2500, 3800, 1, '2021-05-01 19:43:25', '2021-05-01 19:43:25', 4),
(8, 'glasses 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 10, 5000, 7500, 2, '2021-05-01 20:52:45', '2021-05-01 20:52:45', 1),
(9, 'glasses 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 4000, 6000, 2, '2021-05-01 20:52:45', '2021-05-01 20:52:45', 1),
(10, 'glasses 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 2000, 3500, 2, '2021-05-01 20:52:45', '2021-05-01 20:52:45', 1),
(11, 'glasses 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 6, 3000, 3500, 2, '2021-05-01 20:52:45', '2021-05-01 20:52:45', 1),
(12, 'Shoes 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 0, 2200, 1, '2021-05-01 21:16:15', '2021-05-01 21:16:15', 3),
(13, 'Shoes 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 0, 3200, 1, '2021-05-01 21:16:15', '2021-05-01 21:16:15', 3),
(14, 'Shoes 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 0, 4500, 1, '2021-05-01 21:16:15', '2021-05-01 21:16:15', 3),
(15, 'Shoes 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 0, 3500, 1, '2021-05-01 21:16:15', '2021-05-01 21:16:15', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category_name` varchar(100) NOT NULL,
  `product_category_icon_url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_category_id`, `product_category_name`, `product_category_icon_url`) VALUES
(1, 'Women', 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/category_icons%2Fwomen%20dress.svg?alt=media&token=1a029b02-6435-453b-b82a-bd4e3cd19ba2'),
(2, 'Men', 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/category_icons%2Fmen%20dress.svg?alt=media&token=4a4c780f-a1eb-4852-8dcb-5eaffa9f63d3');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image_url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_id`, `product_image_url`) VALUES
(1, 1, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F1%2F1.PNG?alt=media&token=cc5fc7d0-d3e8-487a-9c95-4a1561796381'),
(2, 1, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F1%2F2.PNG?alt=media&token=e6d2fb70-1932-48c2-8fd8-d28c752e2df4'),
(3, 1, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F1%2F3.PNG?alt=media&token=2368526d-b615-4812-bd24-526cefd9f7f6'),
(4, 2, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F2%2Fgreen-jacket1.jpg?alt=media&token=9d1b92be-6793-44e3-af81-0130b0683c21'),
(5, 2, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F2%2Fgreen-jacket2.jpg?alt=media&token=1426d284-f424-4938-a340-217042fef912'),
(6, 2, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F2%2Fgreen-jacket3.jpg?alt=media&token=34244166-fd50-4daf-a49c-9654b3c65e73'),
(7, 3, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F3%2Fbalck-leather-jacket1.jpg?alt=media&token=4d7d23fe-3c84-4233-bc0c-7de7d2303034'),
(8, 3, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F3%2Fbalck-leather-jacket2.jpg?alt=media&token=03bca5a3-9940-456b-977e-ac3ff924da2d'),
(9, 3, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F3%2Fbalck-leather-jacket3.jpg?alt=media&token=05ac32f6-b35d-45aa-bec6-1c2c3501d3be'),
(10, 4, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F4%2Fsuit2-1.jpg?alt=media&token=edf6eacd-4211-4ff5-aa5a-4237ef86287f'),
(11, 4, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F4%2Fsuit2.jpg?alt=media&token=07b02144-5680-4d2c-9688-a015ed313ae9'),
(12, 5, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F5%2Fsuit1-1.jpg?alt=media&token=8765f1bf-d124-4e0a-b3f7-e4ac7b9675bb'),
(13, 5, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F5%2Fsuit1.jpg?alt=media&token=80010d68-44d9-4e39-a2ec-77ebfb7de94a'),
(14, 6, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F6%2Fsuit3-1.jpg?alt=media&token=2f0d11a9-ad5c-4bc0-a8f4-2433e7549398'),
(15, 6, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F6%2Fsuit3-2.jpg?alt=media&token=48ee7556-0fb9-4cb7-8acc-573afe53f86f'),
(16, 7, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F7%2Fsuit4-1.jpg?alt=media&token=cf27fb7d-9792-48b3-82fd-0f29b64fe2cf'),
(17, 7, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F7%2Fsuit4-2.jpg?alt=media&token=441a3cf1-207f-4548-b2b1-558c31aa5cc3'),
(18, 8, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F8%2Fglasses1.png?alt=media&token=e99a8fdc-a9ee-4d6e-9355-31dd765791d8'),
(19, 9, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F9%2Fglasses2-2.jpg?alt=media&token=fed48caa-4f3f-4cdd-b9b9-ae2973cf12ea'),
(20, 9, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F9%2Fglasses2.jpg?alt=media&token=200db520-69b0-4ec0-a18e-06c249b0ee93'),
(21, 10, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F10%2Fglasses3-1.jpg?alt=media&token=20096db2-70ea-4936-9e54-77fbe9abf096'),
(22, 10, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F10%2Fglasses3-2.jpg?alt=media&token=f46d9c79-4a3c-4d88-9ffd-65a19de1bfc4'),
(23, 11, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F11%2Fglasses4-2.jpg?alt=media&token=c1fb6203-5d2e-4673-a80f-df8ef87b8406'),
(24, 11, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F11%2Fglasses4-1.jpg?alt=media&token=3fb4beb4-a7cd-45aa-a91d-760c49c0ffb2'),
(25, 12, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F12%2Fshoes1-1.jpg?alt=media&token=00c4426c-04e9-4109-827f-34334c36bb3a'),
(26, 12, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F12%2Fshoes1-2.jpg?alt=media&token=4b64be5b-2f02-4d2c-bf62-c778037fe835'),
(27, 13, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F13%2Fshoes2-1.jpg?alt=media&token=d87e6b4d-e79c-40d3-9827-3c57bbf6c571'),
(28, 13, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F13%2Fshoes2-2.jpg?alt=media&token=d148aefe-8112-4486-9f14-87ec47dca86f'),
(29, 14, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F14%2Fshoes3-1.jpg?alt=media&token=1806a1ff-e36b-43f9-8bda-1e47077cee0f'),
(30, 14, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F14%2Fshoes3-2.jpg?alt=media&token=96505ec1-6b08-499a-b5cd-99006176e07b'),
(31, 15, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F15%2Fshoes4-1.jpg?alt=media&token=514686c5-c04b-465f-9e77-efe514ca4132'),
(32, 15, 'https://firebasestorage.googleapis.com/v0/b/kapra-bazar.appspot.com/o/products%2F15%2Fshoes4-2.jpg?alt=media&token=843763db-4631-4fb4-a1c7-4df5d349a2c8');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `subcategory_id` int(11) NOT NULL,
  `subcategory_name` varchar(100) NOT NULL,
  `product_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`subcategory_id`, `subcategory_name`, `product_category_id`) VALUES
(1, 'glasses', 2),
(2, 'jackets', 2),
(3, 'shoes', 1),
(4, 'shirts', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usr_id` int(11) NOT NULL,
  `usr_name` varchar(100) NOT NULL,
  `usr_email` varchar(100) NOT NULL,
  `usr_password` varchar(100) NOT NULL,
  `usr_address1` text DEFAULT NULL,
  `usr_address2` text DEFAULT NULL,
  `usr_mobile` varchar(50) DEFAULT NULL,
  `usr_phone` varchar(50) DEFAULT NULL,
  `usr_add_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `usr_modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_usr_active` int(11) NOT NULL DEFAULT 0,
  `usr_hash` int(11) DEFAULT NULL,
  `usr_otp_secret` text NOT NULL,
  `is_otp_active` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usr_id`, `usr_name`, `usr_email`, `usr_password`, `usr_address1`, `usr_address2`, `usr_mobile`, `usr_phone`, `usr_add_date`, `usr_modified_date`, `is_usr_active`, `usr_hash`, `usr_otp_secret`, `is_otp_active`) VALUES
(3, 'saad', 'saadurr@gmail.com', '$2a$10$gYId.dLBvfaRQfKqK23kw.XA3ImPLaza5oq1oQY8UIsqntERjabRG', 'sdkncdsnl@gmail.com', 'dass', '03228540429', NULL, '2020-12-06 16:47:01', '2020-12-23 21:33:41', 1, NULL, '', 0),
(22, 'Saad', 'saadurrehman59@gmail.com', 'none', 'fgefsdgsdgsdg', NULL, '03228540427', NULL, '2021-01-11 20:17:49', '2021-05-02 20:38:40', 1, NULL, 'NNDCSN2UEMZUGJTPKR5DCKCFGFKDOT3JJYUSQIJWERKCILDYMF2A', 1),
(23, 'Saad Ur Rehman', 'saadrehman59@yahoo.com', 'none', NULL, NULL, NULL, NULL, '2021-01-11 21:08:36', '2021-01-11 21:08:36', 1, NULL, '', 0),
(24, 'demooo', 'demo123@demo.com', '$2a$10$8w.eU2bm17gnspE4g8UWUuLjTznVlsAaxa9Tnf3V0O9aYBGxhcFfe', NULL, NULL, NULL, NULL, '2021-01-13 21:40:27', '2021-01-13 21:40:27', 1, NULL, '', 0),
(34, 'admin', 'demi1234@demo.com', '$2a$10$C3euIpy6j0gHz/WlODIVPu4YYx4bch5R5HhXCYzDAAwqOOmeVOhC.', NULL, NULL, NULL, NULL, '2021-01-17 19:53:02', '2021-01-17 19:53:02', 1, NULL, '', 0),
(35, 'enter123', 'enter123@demo.com', '$2a$10$CoqKKvBTg1m4gfw3RhPDHeiNbmSeSN1.ZhAmW6KOSgJOBqXTVu4dO', 'enter123@demo.com', NULL, '23234333455', NULL, '2021-01-17 19:57:30', '2023-03-13 20:18:04', 1, NULL, 'NBXHMZ3ONFRSI5S3PMXDU32XEFMC6UTDJVTHCSLHLB2CYY2XOBKA', 0),
(37, 'admin123', 'admin123@demo.com', '$2a$10$VB6dgqkARjZgeu/RHp5yQuQGN0irGiaTka3J0lr0doESN5FMy6Zou', NULL, NULL, NULL, NULL, '2021-01-17 20:04:39', '2021-01-17 20:04:39', 1, NULL, '', 0),
(49, 'heelelloo', 'iamalansteve@gmail.com', '$2a$10$xGTmuqf6.II4Ds2g3ZOkKO4/M3Fm1.jw20O.nrBvz3EATP/HE/iBG', NULL, NULL, NULL, NULL, '2021-01-22 21:00:54', '2021-01-22 21:01:08', 1, NULL, '', 0),
(52, 'sddadsad', 'saadurrehman@live.co.uk', '$2a$10$OpvE71V0HOqVehOT.62E/uW2Xnq4Km09GH0mp96Ta5stn7pY9fRxC', NULL, NULL, NULL, NULL, '2021-01-22 21:41:30', '2021-01-22 21:45:42', 1, NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_products`
--

CREATE TABLE `user_products` (
  `usr_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `userid` (`user_id`);

--
-- Indexes for table `order_product_detail`
--
ALTER TABLE `order_product_detail`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `productid` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_category_id` (`product_category_id`),
  ADD KEY `sub_category_id` (`product_subcategory_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `prod` (`product_id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`subcategory_id`),
  ADD KEY `produ` (`product_category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usr_id`);

--
-- Indexes for table `user_products`
--
ALTER TABLE `user_products`
  ADD KEY `usr_id` (`usr_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `userid` FOREIGN KEY (`user_id`) REFERENCES `users` (`usr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_product_detail`
--
ALTER TABLE `order_product_detail`
  ADD CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `order_detail` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productid` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `productcategoryid` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`product_category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productsubcategoryd` FOREIGN KEY (`product_subcategory_id`) REFERENCES `sub_category` (`subcategory_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `prod` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD CONSTRAINT `produ` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`product_category_id`);

--
-- Constraints for table `user_products`
--
ALTER TABLE `user_products`
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usr_id` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
