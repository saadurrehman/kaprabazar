const FacebookStrategy = require("passport-facebook").Strategy;
require("dotenv").config();

module.exports = (models, passport) => {
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  passport.use(
    new FacebookStrategy(
      {
        clientID: process.env.FACEBOOK_CLIENT_ID,
        clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
        callbackURL: `${process.env.CLIENT_URL_BACKEND}/facebook/callback`,
        profileFields: ["id", "displayName", "photos", "email"],
      },
      function (accessToken, refreshToken, profile, done) {
        const { email, name } = profile._json;

        const userOptions = {
          where: { usr_email: email },
          defaults: {
            usr_email: email,
            usr_name: name,
            usr_password: "none",
          },
        };

        models.users
          .findOrCreate(userOptions)
          .then((user, err) => {
            const { usr_password, ...rest } = user[0].dataValues;

            return done(err, {
              accessToken,
              refreshToken,
              profile: profile._json,
              user: rest,
              isLoggedWithOTP: false,
              isGoogleAuth: false,
              isFacebookAuth: true,
            });
          })
          .catch((error) => console.error(error));
      }
    )
  );
};
