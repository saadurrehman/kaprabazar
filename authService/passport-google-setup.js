const GoogleStrategy = require("passport-google-oauth20").Strategy;
require("dotenv").config();

module.exports = (models, passport) => {
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: `${process.env.CLIENT_URL_BACKEND}/google/callback`,
      },
      function (accessToken, refreshToken, profile, done) {
        const email = profile._json.email;

        const userOptions = {
          where: { usr_email: email },
          defaults: {
            usr_email: email,
            usr_name: profile.name.givenName,
            usr_password: "none",
            is_usr_active: 1,
            is_otp_active: 0,
            usr_otp_secret: "null",
          },
        };

        models.users
          .findOrCreate(userOptions)
          .then((user, err) => {
            const { usr_password, ...rest } = user[0].dataValues;

            return done(err, {
              accessToken,
              refreshToken,
              profile: profile._json,
              user: rest,
              isLoggedWithOTP: false,
              isGoogleAuth: true,
              isFacebookAuth: false,
            });
          })
          .catch((error) => console.error(error));
      }
    )
  );
};
