const LocalStrategy = require("passport-local").Strategy;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { jwtSecret } = require("../config/config.json");

module.exports = (models, passport) => {
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  passport.use(
    "login",
    new LocalStrategy({ usernameField: "email" }, (email, password, done) => {
      console.log("email::", email);
      console.log("password:::", password);
      models.users
        .findOne({ where: { usr_email: email } })
        .then((user, err) => {
          if (err) return done(null, err);

          if (!user) return done(null, false);

          if (!user.is_usr_active) return done(null, false);

          bcrypt.compare(password, user.usr_password, (err, isUserFound) => {
            console.log(err);

            if (err) return done(null, false);

            if (isUserFound) {
              console.log(isUserFound);

              jwt.sign(
                {
                  id: user.id,
                },

                jwtSecret,
                { expiresIn: 66666 },
                (err, token) => {
                  if (err) done(null, false);
                  const {
                    usr_password,
                    is_usr_active,
                    usr_hash,
                    ...userRestInformation
                  } = user.dataValues;

                  done(null, {
                    token,
                    user: {
                      ...userRestInformation,
                    },
                    isLoggedWithOTP: false,
                    isFacebookAuth: false,
                    isGoogleAuth: false,
                  });
                }
              );
            } else {
              done(null, false);
            }
          });
        });
    })
  );

  passport.use(
    "verify",
    new LocalStrategy({ usernameField: "email" }, (email, password, done) => {
      console.log("email::", email);
      console.log("password:::", password);
      models.users
        .findOne({ where: { usr_email: email } })
        .then((user, err) => {
          if (err) return done(null, err);

          if (!user) return done(null, false);

          jwt.sign(
            {
              id: user.id,
            },

            jwtSecret,
            { expiresIn: 66666 },
            (err, token) => {
              if (err) done(null, false);

              const {
                usr_password,
                is_usr_active,
                usr_hash,
                ...userRestInformation
              } = user.dataValues;
              done(null, {
                token,
                user: {
                  ...userRestInformation,
                },
                isLoggedWithOTP: false,
                isFacebookAuth: false,
                isGoogleAuth: false,
              });
            }
          );
        });
    })
  );
};
