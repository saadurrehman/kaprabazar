const isLoggedIn = (req, res, next) => {
  console.log("USER::::::", req.user);
  if (req.user) {
    next();
  } else {
    res.status(401).json({ message: "unauthorized" });
  }
};

module.exports = { isLoggedIn };
