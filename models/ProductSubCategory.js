const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: false,
    tableName: "sub_category",
  };

  const definition = {
    ["subcategory_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ["subcategory_name"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["product_category_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
  };

  return sequelize.define("sub_category", definition, options);
};
