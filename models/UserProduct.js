const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: false,
    tableName: "user_products",
  };

  const definition = {
    ["usr_id "]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product-quantity"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
  };

  return sequelize.define("user_products", definition, options);
};
