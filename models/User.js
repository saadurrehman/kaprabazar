const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: true,
    tableName: "users",
    createdAt: "usr_add_date",
    updatedAt: "usr_modified_date",
  };

  const definition = {
    ["usr_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ["usr_name"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["is_usr_active"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["usr_hash"]: {
      allowNull: true,
      type: Sequelize.INTEGER,
    },
    ["usr_email"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["usr_password"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["usr_address1"]: {
      allowNull: true,
      type: Sequelize.TEXT,
    },
    ["usr_address2"]: {
      allowNull: true,
      type: Sequelize.TEXT,
    },
    ["usr_mobile"]: {
      allowNull: true,
      type: Sequelize.STRING(50),
    },
    ["usr_phone"]: {
      allowNull: true,
      type: Sequelize.STRING(50),
    },
    ["usr_add_date"]: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    ["usr_otp_secret"]: {
      allowNull: false,
      type: Sequelize.TEXT,
    },
    ["is_otp_active"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["usr_modified_date"]: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  };

  return sequelize.define("users", definition, options);
};
