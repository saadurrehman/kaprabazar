const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: true,
    tableName: "order_detail",
    createdAt: "order_created_at",
    updatedAt: "order_updated_at",
  };

  const definition = {
    ["order_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ["user_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["stripe_payment_id"]: {
      allowNull: true,
      type: Sequelize.TEXT,
    },

    ["order_address"]: {
      allowNull: false,
      type: Sequelize.TEXT,
    },
    ["order_price"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["order_payment_method"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["order_delivery_status"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["order_created_at"]: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    ["order_updated_at"]: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  };

  return sequelize.define("order_detail", definition, options);
};
