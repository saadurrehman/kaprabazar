const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: false,
    tableName: "product_images",
  };

  const definition = {
    ["product_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    ["product_image_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ["product_image_url"]: {
      allowNull: false,
      type: Sequelize.STRING(500),
    },
  };

  return sequelize.define("product_images", definition, options);
};
