const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: true,
    tableName: "products",
    createdAt: "product_add_date",
    updatedAt: "product_modified_date",
  };

  const definition = {
    ["product_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ["product_name"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["product_description"]: {
      allowNull: false,
      type: Sequelize.TEXT,
    },
    ["product_quantity"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_discountprice"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_originalprice"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_originalprice"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_category_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_subcategory_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    ["product_add_date"]: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    ["product_modified_date"]: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  };

  return sequelize.define("products", definition, options);
};
