const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: false,
    tableName: "product_category",
  };

  const definition = {
    ["product_category_id"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ["product_category_name"]: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    ["product_category_icon_url"]: {
      allowNull: false,
      type: Sequelize.STRING(500),
    },
  };

  return sequelize.define("product_category", definition, options);
};
