const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const options = {
    timestamps: false,
    tableName: "order_product_detail",
  };

  const definition = {
    ["product_id"]: {
      allowNull: false,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    ["product_quantity"]: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
  };

  return sequelize.define("order_product_detail", definition, options);
};
