const Products = require("./Products");
const ProductCategory = require("./ProductCategory");
const ProductSubCategory = require("./ProductSubCategory");
const User = require("./User");
const UserProduct = require("./UserProduct");
const ProductImages = require("./ProductImages");
const Orderdetail = require("./Orderdetail");
const OrderProductDetail = require("./OrderProductDetail");
const relationships = require("./relationships");

module.exports = (sequelize) => {
  let models = {
    products: Products(sequelize),
    product_category: ProductCategory(sequelize),
    sub_category: ProductSubCategory(sequelize),
    users: User(sequelize),
    user_products: UserProduct(sequelize),
    product_images: ProductImages(sequelize),
    order_detail: Orderdetail(sequelize),
    order_product_detail: OrderProductDetail(sequelize),
  };
  return relationships(models);
};
