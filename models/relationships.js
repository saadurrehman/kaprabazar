module.exports = (models) => {
  models.products.belongsTo(models.product_category, {
    foreignKey: "product_category_id",
    targetKey: "product_category_id",
  });
  models.products.belongsTo(models.sub_category, {
    foreignKey: "product_subcategory_id",
  });
  models.products.hasMany(models.product_images, {
    foreignKey: "product_id",
    targetKey: "product_id",
  });
  models.product_category.hasMany(models.sub_category, {
    foreignKey: "product_category_id",
    as: "sub_categories",
  });
  models.users.hasMany(models.user_products, {
    foreignKey: "product_id",
    targetKey: "product_id",
  });
  models.order_detail.hasMany(models.order_product_detail, {
    foreignKey: "order_id",
    targetKey: "order_id",
  });
  models.order_product_detail.belongsTo(models.products, {
    foreignKey: "product_id",
    targetKey: "product_id",
  });
  models.users.hasMany(models.order_detail, {
    foreignKey: "user_id",
    targetKey: "user_id",
  });
  return models;
};
