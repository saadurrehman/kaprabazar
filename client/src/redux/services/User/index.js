import { fetchWrapper } from "../../../services/restApi";

export const registerUser = (payload) =>
  fetchWrapper("POST", "/api/user/register", payload);

export const loginUser = (payload) =>
  fetchWrapper("POST", "/api/user/login", payload);

export const logoutUser = () => fetchWrapper("/api/user/logout");

export const getUser = () => fetchWrapper("/api/user/getuser");

export const updateOptimistically = (values) =>
  fetchWrapper("PATCH", "/api/user", values);
