import { actionTypes } from "./actionTypes";

const addProductQuantity = (itemToAdd, cartItems) => {
  if (cartItems.find((item) => item.product_id === itemToAdd.product_id)) {
    return cartItems.map((cart_item) =>
      cart_item.product_id === itemToAdd.product_id
        ? {
            ...cart_item,
            cartProductQuantity: cart_item.cartProductQuantity + 1,
          }
        : cart_item
    );
  }

  return [...cartItems, { ...itemToAdd, cartProductQuantity: 1 }];
};

export const removeProductQuantity = (itemToRemove, cartItems) => {
  const existingCartItem = cartItems.find(
    (item) => item.product_id === itemToRemove.product_id
  );

  if (existingCartItem.cartProductQuantity === 1) {
    return cartItems.filter(
      (item) => item.product_id !== itemToRemove.product_id
    );
  }

  return cartItems.map((cartItem) =>
    cartItem.product_id === itemToRemove.product_id
      ? { ...cartItem, cartProductQuantity: cartItem.cartProductQuantity - 1 }
      : cartItem
  );
};

export const addProductToCart = (product) => (dispatch, getState) => {
  const store = getState();

  dispatch({
    type: actionTypes.ADD_PRODUCT_TO_CART,
    product: addProductQuantity(product, store.product.product),
  });
};

export const reduceProductQuantity = (product) => (dispatch, getState) => {
  const store = getState();

  dispatch({
    type: actionTypes.REDUCE_PRODUCT_QUANTITY,
    product: removeProductQuantity(product, store.product.product),
  });
};

export const removeProductFromCart = (product) => (dispatch) => {
  dispatch({ type: actionTypes.REMOVE_PRODUCT_FROM_CART, product });
};
