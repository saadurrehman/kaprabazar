import { actionTypes } from "./actionTypes";
import { fetchWrapper } from "services/restApi";
import * as service from "../../services/User";
import toastr from "toastr";

//When the User got loggedin by using GoogleAuth
export const setCurrentUser = (user) => (dispatch) => {
  dispatch({ type: actionTypes.SET_CURRENT_USER, user });
};

export const registerUser = (payload) => async (dispatch) => {
  try {
    const user = await service.registerUser(payload);

    // dispatch({ type: actionTypes.SET_CURRENT_USER, user });

    toastr.success("Please Check Your Email");
    return user;
  } catch (error) {
    toastr.error(error.message, "Error");
  }
};

export const loginUser = (payload) => async (dispatch) => {
  try {
    const user = await service.loginUser(payload);

    dispatch({ type: actionTypes.SET_CURRENT_USER, user });

    toastr.success("User Login successfully");
    return user;
  } catch (error) {
    toastr.error(error.message, "Error");
  }
};

export const updateOptimistically = (values) => async (dispatch, getState) => {
  const prevState = getState().currentUser.user;

  try {
    await service.updateOptimistically(values);
    dispatch({ type: actionTypes.UPDATE_OPTIMISTICALLY_USER_SUCCESS, values });
  } catch (error) {
    toastr.error(error.message);

    dispatch({
      type: actionTypes.UPDATE_OPTIMISTICALLY_USER_FAILED,
      prevState,
    });
  }
};

export const getUser = () => async (dispatch, getState) => {
  const ifExistAlready = getState().currentUser?.user || null;

  if (ifExistAlready) return;

  try {
    const user = await service.getUser();
    dispatch({ type: actionTypes.SET_CURRENT_USER, user });
  } catch (error) {}
};

export const logoutUser = () => async (dispatch) => {
  try {
    const { logout } = await service.logoutUser();
    if (logout) {
      dispatch({ type: actionTypes.LOGOUT_CURRENT_USER });
    }
  } catch (error) {
    console.error(error);
  }
};

export const twoFactorAuthenticationStatus = () => async (dispatch) => {
  try {
    dispatch({ type: actionTypes.TWO_FACTOR_STATUS });
  } catch (error) {
    console.error(error);
  }
};
