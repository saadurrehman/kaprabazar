import { actionTypes } from "./actionTypes";

export const checkoutProduct = () => (dispatch) => {
  dispatch({
    type: actionTypes.REMOVE_ALL_PRODUCTS,
    product: [],
  });
};
