import { actionTypes } from "../../actions/User/actionTypes";

const INITIAL_STATE = {
  user: null,
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionTypes.SET_CURRENT_USER:
      return {
        ...state,
        ...action.user,
      };

    case actionTypes.UPDATE_OPTIMISTICALLY_USER_FAILED:
      return {
        ...state,
        ...action.prevState,
      };

    case actionTypes.TWO_FACTOR_STATUS:
      return {
        ...state,
        isLoggedWithOTP: true,
        user: { ...state.user, is_otp_active: 1 },
      };

    case actionTypes.UPDATE_OPTIMISTICALLY_USER_SUCCESS:
      return {
        ...state,
        user: { ...state.user, ...action.values },
      };

    case actionTypes.LOGOUT_CURRENT_USER:
      return null;

    default:
      return state;
  }
};

export default userReducer;
