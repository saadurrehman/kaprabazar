import { actionTypes } from "../../actions/Product/actionTypes";

const INITIAL_STATE = {
  product: [],
};

const removeProduct = (product, productToRemove) => {
  return product.filter(
    (item) => item.product_id !== productToRemove.product_id
  );
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionTypes.ADD_PRODUCT_TO_CART:
    case actionTypes.REDUCE_PRODUCT_QUANTITY:
      return {
        ...state,
        product: action.product,
      };

    case actionTypes.REMOVE_PRODUCT_FROM_CART:
      return {
        ...state,
        product: removeProduct(state.product, action.product),
      };

    case actionTypes.REMOVE_ALL_PRODUCTS:
      return {
        ...state,
        product: action.product,
      };

    default:
      return state;
  }
};

export default userReducer;
