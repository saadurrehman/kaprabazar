import React from "react";
import CheckoutForm from "../components/Client/Checkout/CheckoutForm";
import CheckoutDetails from "../components/Client/Checkout/CheckoutDetails";
import "./Checkout.css";

const Checkout = () => {
  return (
    <div className="checkout-wrapper">
      <div className="checkout-container">
        <CheckoutForm />
        <CheckoutDetails />
      </div>
    </div>
  );
};

export default Checkout;
