import TokenField from "components/Client/Common/TokenField";
import React from "react";
import toastr from "toastr";
import { useNavbarDispatch } from "contextApi/navbarContextapi";
// import Modal from "@material-ui/core/Modal";
import { Col, Button, Modal } from "react-bootstrap";
import { useSelector } from "react-redux";
import { fetchWrapper } from "services/restApi";

function TwoFactorAuth() {
  const [showotpNumber, setShowOTPNumber] = React.useState(false);
  const [otpShow, setOTPShow] = React.useState(null);
  const [secret, setSecret] = React.useState("");
  const currentUser = useSelector((state) => state.currentUser);
  const { setZIndex } = useNavbarDispatch();

  const handleQRCodeGenerator = async () => {
    try {
      const { secret } = await fetchWrapper("POST", "/api/user/register/otp", {
        email: currentUser.user.usr_email,
      });

      if (secret) {
        setSecret(secret);
        setOTPShow(true);
      }
    } catch (error) {
      toastr.error(error);
      setZIndex(9999);
    }
  };

  React.useEffect(() => {
    setZIndex(11);
  }, [setZIndex]);

  return (
    <>
      {otpShow && (
        <Modal
          size="md"
          show={() => setOTPShow(true)}
          onHide={() => setOTPShow(false)}
          aria-labelledby="example-modal-sizes-title-sm"
          centered
        >
          <Modal.Header closeButton>Two Factor Authentication</Modal.Header>
          <Modal.Body
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h2 id="modal-title">
              {!showotpNumber ? "Scan QR code" : "Enter 6 digit code"}
            </h2>
            <hr />
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                maxHeight: "400px",
              }}
            >
              {showotpNumber && (
                <TokenField
                  setShowOTPNumber={setShowOTPNumber}
                  email={currentUser.user.usr_email}
                  setZIndex={setZIndex}
                />
              )}
              {!showotpNumber && (
                <>
                  {secret ? (
                    <>
                      <img
                        style={{ alignSelf: "center" }}
                        alt="barcode"
                        width="300"
                        height="300"
                        src={`https://chart.googleapis.com/chart?chs=200x200&chld=M%7C0&cht=qr&chl=otpauth://totp/Practice_OTP?secret=${secret}`}
                      />
                      <Button
                        className="btn mb-3"
                        onClick={() => setShowOTPNumber(true)}
                        style={{
                          backgroundColor: "var(--primary-color)",
                          border: "none",
                        }}
                      >
                        Next Step
                      </Button>
                    </>
                  ) : (
                    <p>Loading QR Code</p>
                  )}
                </>
              )}
            </div>
          </Modal.Body>
        </Modal>
      )}
      <Col className="d-xs-block order-wrapper">
        <h4>Two Factor Authentication </h4>
        <hr />
        <div className="mb-4">
          <p>
            Kapra Bazar is providing two factor authentication to its customers.
          </p>
          <p>The main purpose of this feature is to secure our user.</p>
          <p>So that they can shop from our website with full safety</p>
          <p>Click the button below to Turn on this feature</p>
          <div>
            {!currentUser.user.is_otp_active ? (
              <Button
                className="btn"
                onClick={() => handleQRCodeGenerator()}
                style={{
                  backgroundColor: "var(--primary-color)",
                  border: "none",
                }}
              >
                Two Factor authentication
              </Button>
            ) : (
              <p>Your two factor authentication is active</p>
            )}
          </div>
        </div>
      </Col>
    </>
  );
}

export default TwoFactorAuth;
