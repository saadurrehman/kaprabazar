import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "./login/Login";
import Dashboard from "./dashboard/Dashboard";
const AdminHome = (props) => {
  return (
    <>
      <Route path={props.match.url + "/login"}>
        <Login />
      </Route>
      <Route
        path={props.match.url + "/"}
        render={(props) => <Dashboard {...props} />}
      />
    </>
  );
};
export default AdminHome;
