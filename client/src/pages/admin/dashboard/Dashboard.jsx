import React from "react";
import Navbar from "../../../components/Admin/Navbar/Navbar";
import MUIDataTable from "mui-datatables";
import SideMenu from "../../../components/Admin/SideMenu/SideMenu";
import "./Dashboard.css";

const Dashboard = ({ history, match }) => {
  const [title, setTitle] = React.useState("");
  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "company",
      label: "Company",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "city",
      label: "City",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "state",
      label: "State",
      options: {
        filter: true,
        sort: false,
      },
    },
  ];

  const data = [
    { name: "Joe James", company: "Test Corp", city: "Yonkers", state: "NY" },
    { name: "John Walsh", company: "Test Corp", city: "Hartford", state: "CT" },
    { name: "Bob Herm", company: "Test Corp", city: "Tampa", state: "FL" },
    {
      name: "James Houston",
      company: "Test Corp",
      city: "Dallas",
      state: "TX",
    },
  ];

  const options = {
    filterType: "checkbox",
  };

  React.useEffect(() => {
    const { pathname } = history.location;

    if (pathname === "/admin/dashboard") {
      //fetch product data
      setTitle("Products List");
      return;
    }
    if (pathname === "/admin/orders") {
      //fetch orders data
      setTitle("Orders List");
      return;
    }
    if (pathname === "/admin/category") {
      //fetch category data
      setTitle("Category List");
      return;
    }
    if (pathname === "/admin/customers") {
      //fetch customer data
      setTitle("Customers List");
      return;
    }
  }, [history.location]);

  return (
    <>
      <Navbar />
      <div className="maincontent">
        <SideMenu />
        {/* Create a Component for that */}
        <h1 className="ml-5 mt-5" style={{ width: "100%" }}>
          <MUIDataTable
            title={title}
            data={data} //this data is dynamic and fetching data when the url changes
            columns={columns}
            options={options}
          />
        </h1>
      </div>
    </>
  );
};

export default Dashboard;
