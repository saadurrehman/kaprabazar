import React from "react";
import { withRouter, Redirect } from "react-router-dom";
import { Row, Container } from "react-bootstrap";
import SidebarProfileOrderPage from "../../components/Client/SidebarProfileOrderPage/SidebarProfileOrderPage";
import Profile from "../Profile/Profile";
import Order from "../Orders/Order";
import TwoFactorAuth from "pages/TwoFactorAuth/TwoFactorAuth";

const UserSettings = ({ history, match }) => {
  if (history.location.pathname === "/user") {
    return <Redirect to="/user/profile" />;
  }
  return (
    <Container>
      <Row>
        <SidebarProfileOrderPage />
        {history.location.pathname === "/user/profile" && <Profile />}
        {history.location.pathname === "/user/orders" && <Order />}
        {history.location.pathname === "/user/two-factor-auth" && (
          <TwoFactorAuth />
        )}
      </Row>
    </Container>
  );
};

export default withRouter(UserSettings);
