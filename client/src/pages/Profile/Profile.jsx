import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row, Form } from "react-bootstrap";
import ContactCard from "../../components/Client/Common/ContactCard/ContactCard";
import { updateOptimistically } from "redux/actions/User";
import "./Profile.css";

const Profile = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.currentUser);
  const email = currentUser.user.email;
  const currentValue = currentUser.user.usr_name;
  const [userName, setUserName] = React.useState(currentValue || "");

  const optimisticUpdateUserInfo = async (e) => {
    dispatch(updateOptimistically({ [e.target.name]: e.target.value, email }));
  };

  return (
    <>
      <Col className="d-xs-block profile-container">
        <h2>Your Profile </h2>
        <div className="mb-4">
          <Form className="profile__personalInfoForm">
            <Row className="personalInfoForm__row">
              <Col xs={12} sm={10} lg={10}>
                <Form.Group controlId="formBasicName">
                  <Form.Label>Your Name</Form.Label>
                  <Form.Control
                    value={userName}
                    type="text"
                    name="usr_name"
                    onBlur={optimisticUpdateUserInfo}
                    onChange={(e) => setUserName(e.target.value)}
                    placeholder="Enter your name"
                  />
                </Form.Group>
              </Col>
            </Row>
          </Form>
        </div>
        <div className="mb-4">
          <h6>Contact Number </h6>
          <div className="profile__cardWrapper">
            <ContactCard
              title="Phone"
              fieldName="usr_phone"
              currentUser={currentUser}
            />
            <ContactCard
              fieldName="usr_mobile"
              title="Mobile"
              currentUser={currentUser}
            />
          </div>
        </div>
        <div className="mb-4">
          <h6>Delivery Address</h6>
          <div className="profile__cardWrapper">
            <ContactCard
              title="Address 1"
              fieldName="usr_address1"
              currentUser={currentUser}
              className="profile__contactCard"
            />

            <ContactCard
              title="Address 2"
              fieldName="usr_address2"
              currentUser={currentUser}
              className="profile__contactCard"
            />
          </div>
        </div>
      </Col>
    </>
  );
};

export default Profile;
