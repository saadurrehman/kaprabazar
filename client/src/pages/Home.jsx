import React from "react";
import { useSelector } from "react-redux";
import Slider from "../components/Client/Slider/Slider";
import Header from "../components/Client/Header/Header";
import CartPopUpBoxButton from "../components/Client/Cart/CartPopUpBoxButton";
import MainContent from "../components/Client/ContentContainer/MainContent";
import CartSlidePopUp from "../components/Client/Cart/CartSlidePopUp";

const Home = ({
  openSearchModel,
  handleCloseSearchModal,
  handleOpenSearchModal,
}) => {
  const [isOpenCart, setIsOpenCart] = React.useState(false);
  const { product } = useSelector((state) => state.product);

  const openAndCloseCart = () => {
    setIsOpenCart(!isOpenCart);
  };
  return (
    <>
      <Header
        openSearchModel={openSearchModel}
        handleCloseSearchModal={handleCloseSearchModal}
        handleOpenSearchModal={handleOpenSearchModal}
      />
      <Slider />

      <CartPopUpBoxButton product={product} openCart={openAndCloseCart} />
      {isOpenCart && (
        <CartSlidePopUp product={product} closeCart={openAndCloseCart} />
      )}
      <MainContent />
    </>
  );
};

export default Home;
