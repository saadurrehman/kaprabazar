import React from "react";
import OrderCard from "../../components/Client/Order-Card/Order-Card";
import { Col } from "react-bootstrap";
import "./Order.css";
import { useFetch } from "hooks/useFetch";
import { useSelector } from "react-redux";

const Order = () => {
  const { data, setUrl } = useFetch();
  const currentUser = useSelector((state) => state.currentUser);

  React.useEffect(() => {
    async function getOrders() {
      setUrl(`/api/order/${currentUser.user.usr_id}`);
    }

    getOrders();
  }, [setUrl, currentUser.user.usr_id]);

  return (
    <>
      <Col className="d-xs-block order-wrapper">
        <h4>Your Order </h4>
        <hr />
        <div className="mb-4">
          <div className="order-list___wrapper">
            {Array.isArray(data) && data.length ? (
              data.map((item) => <OrderCard item={item} />)
            ) : (
              <h3 className="order-list__no-order-available">
                No Order Available
              </h3>
            )}
          </div>
        </div>
      </Col>
    </>
  );
};

export default Order;
