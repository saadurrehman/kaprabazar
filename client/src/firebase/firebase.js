import firebase from "firebase";
import "firebase/firestore";
import "firebase/storage";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyDIasGd0ZZg4qDUl2E88EdFhoU2LClU-ZY",
  authDomain: "kapra-bazar.firebaseapp.com",
  databaseURL: "https://kapra-bazar.firebaseio.com",
  projectId: "kapra-bazar",
  storageBucket: "kapra-bazar.appspot.com",
  messagingSenderId: "1002794183803",
  appId: "1:1002794183803:web:644b357f449234324486de",
  measurementId: "G-XDZW23RV5Q",
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const storage = firebase.storage();

// const provider = new firebase.auth.GoogleAuthProvider();
// provider.setCustomParameters({ prompt: "select_account" });

// export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
