import React from "react";

const NavbarShowContext = React.createContext();
const NavbarShowDispatchContext = React.createContext();

function NavbarProvider({ children }) {
  const [zIndex, setZIndex] = React.useState(9999);

  return (
    <NavbarShowContext.Provider value={zIndex}>
      <NavbarShowDispatchContext.Provider value={{ setZIndex }}>
        {children}
      </NavbarShowDispatchContext.Provider>
    </NavbarShowContext.Provider>
  );
}

function useNavbarIsShown() {
  const context = React.useContext(NavbarShowContext);
  if (context === undefined) {
    throw new Error("useNavbarIsShown must be used within a NavbarProvider");
  }
  return context;
}

function useNavbarDispatch() {
  const context = React.useContext(NavbarShowDispatchContext);
  if (context === undefined) {
    throw new Error("useNavbarDispatch must be used within a NavbarProvider");
  }
  return context;
}

function useNavbar() {
  return [useNavbarIsShown(), useNavbarDispatch()];
}

export { NavbarProvider, useNavbar, useNavbarIsShown, useNavbarDispatch };
