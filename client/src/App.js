import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useDialog } from "./hooks/useDialog";
// import { IdleTimeoutManager } from "idle-timer-manager";
import { Switch, Route, withRouter } from "react-router-dom";
import Home from "./pages/Home";
import Checkout from "./pages/Checkout";
import AdminHome from "./pages/admin/Home";
import UserSettings from "./pages/UserSettings/UserSettings";
import Navbar from "components/Client/Navbar/Navbar";
import ProtectedRoute from "components/Client/Common/ProtectedRoute";
import { getUser } from "redux/actions/User";
import { useNavbarDispatch } from "contextApi/navbarContextapi";
import "./App.css";
import "toastr/build/toastr.css";
import TwoFactorAuth from "components/Client/TwoFactorAuth/TwoFactorAuth";

function App() {
  const dispatch = useDispatch();
  const { setZIndex } = useNavbarDispatch();
  const currentUser = useSelector((state) => state.currentUser);
  const [
    isSearchModalOpen,
    { openDialog: openSearchModal, closeDialog: closeSearchModal },
  ] = useDialog();

  const handleOpenSearchModal = () => {
    setZIndex(-9999);
    openSearchModal();
  };

  const handleCloseSearchModal = () => {
    closeSearchModal();
  };

  React.useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  return (
    <div className="app">
      <Switch>
        {" "}
        <Route path="/admin" render={(props) => <AdminHome {...props} />} />
        <Route>
          <Navbar handleOpenSearchModal={handleOpenSearchModal} />
          <Route path="/checkout">
            <ProtectedRoute component={Checkout} />
          </Route>
          <Route path="/user">
            <ProtectedRoute component={UserSettings} />
          </Route>
          <Route exact path="/">
            <Home
              openSearchModel={isSearchModalOpen}
              handleCloseSearchModal={handleCloseSearchModal}
              handleOpenSearchModal={handleOpenSearchModal}
            />
            {currentUser?.user?.is_otp_active === 1 &&
              !currentUser?.isLoggedWithOTP && (
                <TwoFactorAuth
                  setZIndex={setZIndex}
                  currentUser={currentUser}
                />
              )}
          </Route>
        </Route>
      </Switch>
    </div>
  );
}

export default withRouter(App);
