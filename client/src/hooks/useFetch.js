import React from "react";
import toastr from "toastr";
import { fetchWrapper } from "../services/restApi";

/**
 * @description Custom hook to fetch data
 * @example useFetch('/api/sqs/user');
 * @param {string} [initialUrl] - can be a string or null, initialize with null if you don't want the fetch to happen immediately
 * @param {string} [toastrErrorMessage] - can be a string or null, will show toastr error if there is an error loading data.  Leave null to handle displaying error message inline instead.
 * @returns {{ data: *, isLoading: Boolean, errorMessage: String, setUrl: (url:string)=>void }}
 */
export function useFetch(
  initialUrl,
  toastrErrorMessage = "There has been an error loading data."
) {
  const [url, setUrl] = React.useState(initialUrl);
  const [data, setData] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");

  const fetchData = React.useCallback(async () => {
    try {
      setIsLoading(true);
      setErrorMessage("");
      const results = await fetchWrapper(url);
      setData(results);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setErrorMessage(error.message || error);
      toastr.error(error.message);
    }
  }, [url]);

  const resetState = React.useCallback(() => {
    setUrl(initialUrl);
    setData(null);
    setIsLoading(false);
    setErrorMessage("");
  }, [initialUrl]);

  React.useEffect(() => {
    if (url) fetchData();
  }, [url, fetchData]);

  return { data, isLoading, errorMessage, setUrl, resetState };
}
