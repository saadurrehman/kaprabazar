import { useState } from "react";

export function useDialog(isOpen = false) {
  const [isDialogOpen, setIsDialogOpen] = useState(isOpen);

  const openDialog = () => {
    setIsDialogOpen(true);
  };

  const closeDialog = () => {
    setIsDialogOpen(false);
  };

  return [isDialogOpen, { openDialog, closeDialog }];
}
