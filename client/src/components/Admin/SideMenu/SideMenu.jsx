import React from "react";
import "./SideMenu.css";
import DashboardIcon from "@material-ui/icons/Dashboard";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import CategoryIcon from "@material-ui/icons/Category";
import PeopleIcon from "@material-ui/icons/People";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { NavLink } from "react-router-dom";

const SideMenu = () => {
  return (
    <div className="sidebar-wrapper">
      <div className="sidebar-container">
        <span>
          <DashboardIcon />
          <NavLink to="/admin/dashboard" activeClassName="sidebar-active">
            Products
          </NavLink>
        </span>
        <span>
          <LocalMallIcon />
          <NavLink to="/admin/category" activeClassName="sidebar-active">
            Category
          </NavLink>{" "}
        </span>
        <span>
          <CategoryIcon />
          <NavLink to="/admin/orders" activeClassName="sidebar-active">
            Orders
          </NavLink>
        </span>
        <span>
          <PeopleIcon />
          <NavLink to="/admin/customers" activeClassName="sidebar-active">
            Customer
          </NavLink>
        </span>
        <hr className="sidebar-divider" />
        <span>
          <ExitToAppIcon />
          <NavLink to="/admin/logout">Logout</NavLink>
        </span>
      </div>
    </div>
  );
};

export default SideMenu;
