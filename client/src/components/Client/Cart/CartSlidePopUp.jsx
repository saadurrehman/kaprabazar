import React from "react";
import { Link } from "react-router-dom";
import { useTotalAmountOfCartProducts } from "./CartPopUpBoxButton";
import CartProductList from "./CartProductList";
import emptyCart from "images/cart/emptyCart.png";
import "./CartSlidePopUp.css";

const CartSlidePopUp = ({ closeCart, product }) => {
  const { totalAmount } = useTotalAmountOfCartProducts(product);

  return (
    <div className="cart-list-popup">
      <div className="cart-list-popup__body">
        <div className="cart-list-popup__header">
          <div className="cart-list-popup__item-count">
            <i className="fa fa-shopping-bag"></i>
            <span className="ml-2">{product.length} item</span>
          </div>
          <button className="cart-list-popup__close-btn">
            <i onClick={closeCart} className="fa fa-times-circle"></i>
          </button>
        </div>
        <div>
          <div className="cart-list-popup__show-item-wrapper">
            <div className="cart-list-popup__show-item-body">
              <div className="item-wrapper">
                {product.length ? (
                  product.map((product) => (
                    <CartProductList product={product} />
                  ))
                ) : (
                  <div className="item-wrapper__no-product-img-wrapper">
                    <img
                      src={emptyCart}
                      alt="emptycart"
                      className="item-wrapper__no-product-img"
                    />
                    <span className="item-wrapper__no-item">
                      No Products Found
                    </span>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="cart-list-popup__checkout-btn-wrapper">
          {product.length > 0 && (
            <button className="cart-list-popup__checkout-btn">
              <Link
                to="/checkout"
                className="cart-list-popup__checkout-btn-title"
              >
                Checkout
              </Link>
              <span className="cart-list-popup__price-box">
                $ {totalAmount}
              </span>
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default CartSlidePopUp;
