import React from "react";
import "./cartpopupboxbutton.css";

export const useTotalAmountOfCartProducts = (product) => {
  const totalAmount = React.useMemo(() => {
    return product.reduce((acc, item) => {
      const priceToAdd = item.product_discountprice
        ? item.product_discountprice
        : item.product_originalprice;
      return acc + priceToAdd;
    }, 0);
  }, [product]);

  return { totalAmount };
};

const CartPopUpBoxButton = ({ openCart, product }) => {
  const { totalAmount } = useTotalAmountOfCartProducts(product);

  return (
    <>
      <button className="cartButton" onClick={openCart}>
        <span className="cartButton__cartItem">
          <i className="fa fa-shopping-cart cartButton__cartIcon"></i>
          {product.length} Item
        </span>
        <span className="cartButton__cartItemPrice">$ {totalAmount}</span>
      </button>
    </>
  );
};

export default CartPopUpBoxButton;
