import { Typography } from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import {
  addProductToCart,
  removeProductFromCart,
  reduceProductQuantity,
} from "redux/actions/Product";
import LoadImage from "../Common/LoadImage";
import "./CartProductList.css";

const getPriceOfProduct = (product) => {
  return product.product_discountprice
    ? product.product_discountprice
    : product.product_originalprice;
};

const totalPrice = (product) => {
  return parseInt(product.cartProductQuantity * getPriceOfProduct(product));
};

function CartProductList({ product }) {
  const dispatch = useDispatch();

  const addQuantity = () => {
    dispatch(addProductToCart(product));
  };

  const removeQuantity = () => {
    dispatch(reduceProductQuantity(product));
  };

  const _removeProductFromCart = () => {
    dispatch(removeProductFromCart(product));
  };

  return (
    <div className="cart-item__header">
      <div className="cart-item__counter">
        <button onClick={removeQuantity} className="cart-item__add-quantity">
          -
        </button>
        <span className="">1</span>
        <button onClick={addQuantity} className="cart-item__add-quantity">
          +
        </button>
      </div>
      <LoadImage
        imgClass="cart-item__product-image"
        url={product?.product_images[0]?.product_image_url}
      />
      <div className="cart-item__info-section">
        <Typography className="cart-item__product-name">
          {product.product_name}
        </Typography>
        <Typography className="cart-item__product-price">
          $
          {product.product_discountprice
            ? product.product_discountprice
            : product.product_originalprice}
        </Typography>
        <Typography className="cart-item__product-quantity">
          {product.cartProductQuantity} pc(s)
        </Typography>
      </div>
      <Typography className="cart-item__product-original-price">
        ${totalPrice(product)}
      </Typography>
      <button
        onClick={_removeProductFromCart}
        className="cart-item__remove-product"
      >
        X
      </button>
    </div>
  );
}

export default CartProductList;
