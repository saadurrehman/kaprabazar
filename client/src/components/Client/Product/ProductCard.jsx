import React from "react";
import ProductCardList from "./ProductCardList";
import { addProductToCart } from "redux/actions/Product";
import ProductCardModal from "./ProductCardModal";
import { useDispatch } from "react-redux";
import "react-multi-carousel/lib/styles.css";
import "./ProductCard.css";

const ProductCard = ({ product }) => {
  const dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const toggleShowcaseModal = () => setIsModalOpen(!isModalOpen);

  const getDiscountedPrice = React.useMemo(() => {
    const { product_discountprice, product_originalprice } = product || {};
    if (product_discountprice === 0) return 0;

    const difference = product_discountprice / product_originalprice; //0.8

    const discountPercentage = parseInt(100 - difference * 100);
    return discountPercentage;
  }, [product.product_discountprice, product.product_originalprice]);

  const handleAddProductToCart = () => {
    dispatch(addProductToCart(product));
  };

  return (
    <>
      <ProductCardModal
        discountedPrice={getDiscountedPrice}
        isModalOpen={isModalOpen}
        toggleShowcaseModal={toggleShowcaseModal}
        product={product}
        handleAddProductToCart={handleAddProductToCart}
      />
      <ProductCardList
        discountedPrice={getDiscountedPrice}
        toggleShowcaseModal={toggleShowcaseModal}
        product={product}
        handleAddProductToCart={handleAddProductToCart}
      />
    </>
  );
};

export default ProductCard;
