import React from "react";
import ProductCard from "./ProductCard";
import GlobalLoader from "../Common/GlobalLoader";
import { useFetch } from "hooks/useFetch";
import "./ProductsSection.css";

const ProductsSection = ({ categoryID, subCategoryID }) => {
  const { data, isLoading, setUrl } = useFetch();

  React.useEffect(() => {
    const getProducts = () => {
      setUrl(`/api/products/${categoryID}/${subCategoryID}`);
    };

    getProducts();
  }, [setUrl, categoryID, subCategoryID]);

  return (
    <div id="products" className="content-section">
      <div className="product-list-style">
        {isLoading ? (
          <GlobalLoader />
        ) : (
          data?.products.map((item) => <ProductCard product={item} />)
        )}
      </div>
    </div>
  );
};

export default ProductsSection;
