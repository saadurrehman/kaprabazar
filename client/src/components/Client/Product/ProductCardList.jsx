import React from "react";
import { Typography } from "@material-ui/core";
import Zoom from "react-reveal/Zoom";
import LoadImage from "../Common/LoadImage";

function ProductCardList({ toggleShowcaseModal,handleAddProductToCart, product, discountedPrice }) {
 
  return (
    <div className="product-list-style__products-column">
      <div className="product-list-style__products-card-wrapper">
        <Zoom>
          <div className="product-list-style__book-card-wrapper">
            <div
              className="product-list-style__book-image-wrapper"
              onClick={toggleShowcaseModal}
            >
              <LoadImage
                imgClass="product-list-style__book-image"
                url={product?.product_images[0]?.product_image_url}
              />
              {discountedPrice !== 0 && (
                <Typography
                  variant="body1"
                  className="product-list-style__show-discount"
                >
                  {discountedPrice}%
                </Typography>
              )}
            </div>
            <div className="product-list-style__book-info-wrapper">
              <Typography
                variant="body1"
                className="product-list-style__product-name"
              >
                {product.product_name}
              </Typography>
              <Typography
                variant="body1"
                className="product-list-style__product-weight"
              >
                {`${product.product_quantity}`} pc(s)
              </Typography>
              <div className="product-list-style__product-meta">
                <div className="product-list-style__product-price-wrapper">
                  {discountedPrice !== 0 ? (
                    <>
                      <Typography
                        variant="body1"
                        className="product-list-style__product-discount-price"
                      >
                        ${product.product_originalprice}
                      </Typography>
                      <Typography
                        variant="body1"
                        className="product-list-style__product-price"
                      >
                        ${product.product_discountprice}
                      </Typography>
                    </>
                  ) : (
                    <>
                      <Typography
                        variant="body1"
                        className="product-list-style__product-price"
                      >
                        ${product.product_originalprice}
                      </Typography>
                    </>
                  )}
                </div>
                <button
                  onClick={handleAddProductToCart}
                  className="product-list-style__add-to-cart-button"
                >
                  <i className="fa fa-shopping-cart product-list-style__carticon"></i>
                  <span className="add-to-cart-button__btntext">Cart</span>
                </button>
              </div>
            </div>
          </div>
        </Zoom>
      </div>
    </div>
  );
}

export default ProductCardList;
