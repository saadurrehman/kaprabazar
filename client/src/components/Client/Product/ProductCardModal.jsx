import React from "react";
import { Typography } from "@material-ui/core";
import CarouselWithCustomDots from "react-multi-carousel";
import { Modal, Row, Col, Container } from "react-bootstrap";
import LoadImage from "../Common/LoadImage";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 1,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

function ProductCardModal({
  isModalOpen,
  toggleShowcaseModal,
  product,
  handleAddProductToCart,
  discountedPrice,
}) {
  return (
    <Modal
      show={isModalOpen}
      backdrop="static"
      keyboard={false}
      onHide={toggleShowcaseModal}
      centered
      size="lg"
    >
      <Modal.Header closeButton>{product.product_name}</Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6} xs={12}>
              <CarouselWithCustomDots
                responsive={responsive}
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlaySpeed={1000}
                transitionDuration={500}
                slidesToSlide={1}
                arrows={false}
                itemClass="carousel-item-padding-40-px"
                renderDotsOutside={true}
                showDots
              >
                {product?.product_images?.map((item) => (
                  <LoadImage
                    imgClass="product-image-carosuel"
                    url={item.product_image_url}
                  />
                ))}
              </CarouselWithCustomDots>
              {discountedPrice !== 0 && (
                <Typography className="product-list-style__show-discount">
                  {discountedPrice}%
                </Typography>
              )}
            </Col>
            <Col md={6} xs={12}>
              <Row>
                <Col md={8}>
                  <Typography
                    variant="body2"
                    className="product-list-style__product-name"
                  >
                    {product.product_name}
                  </Typography>
                </Col>
                <Col md={4}>
                  {discountedPrice !== 0 ? (
                    <>
                      <Typography
                        variant="body1"
                        className="product-Modal-style__product-discount-price"
                      >
                        ${product.product_discountprice}
                      </Typography>
                      <Typography
                        variant="body1"
                        className="product-Modal-style__product-price"
                      >
                        ${product.product_originalprice}
                      </Typography>
                    </>
                  ) : (
                    <>
                      <Typography
                        variant="body1"
                        className="product-Modal-style__product-price"
                      >
                        ${product.product_originalprice}
                      </Typography>
                    </>
                  )}
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <Typography
                    variant="body1"
                    className="product-list-style__product-weight"
                  >
                    {`${product.product_quantity}`} Pc(s)
                  </Typography>
                </Col>
                <Col>
                  <Typography variant="body1">
                    {" "}
                    {product.product_description}
                  </Typography>
                </Col>
              </Row>
              <Row className="productModal__cartIcon">
                <button className="product-list-style__add-to-cart-button" onClick={handleAddProductToCart}>
                  <i className="fa fa-shopping-cart product-list-style__carticon"></i>
                  <span className="add-to-cart-button__btntext">Cart</span>
                </button>
              </Row>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
}

export default ProductCardModal;
