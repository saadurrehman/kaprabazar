import React from "react";
import SideBarContent from "../Sidebar/SideBarContent";
import GlobalLoader from "../Common/GlobalLoader";
import "./ResponsiveCategoryContent.css";

const ResponsiveCategoryContent = ({ isLoading, categories }) => {
  return (
    <div className="popover-content">
      <div className="inner-wrap">
        {isLoading && <GlobalLoader />}

        {categories?.map((item) => (
          <SideBarContent
            imgUrl={item.product_category_icon_url}
            key={item.product_category_id}
            subCategory={item.sub_categories}
            categoryName={item.product_category_name}
          />
        ))}
      </div>
    </div>
  );
};

export default ResponsiveCategoryContent;
