import React from "react";
import "./ResponsiveCategoryButton.css";
import ResponsiveCategoryContent from "./ResponsiveCategoryContent";

const ResponsiveCategoryButton = ({ isLoading, categories }) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const handleShowModal = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <div className="popover-handler" onClick={handleShowModal}>
        <div className="sidebarstyle__popover-handler">
          <div>
            <i className="fa fa-th-large pop-over-handler__icon"></i>
            Select your Category
            <i className="fa fa-arrow-down pop-over-handler__dropdownarrow"></i>
          </div>
        </div>
      </div>
      {isOpen && (
        <ResponsiveCategoryContent
          isLoading={isLoading}
          categories={categories}
        />
      )}
    </>
  );
};

export default ResponsiveCategoryButton;
