import React from "react";
import SelectField from "components/Client/Common/SelectField";
import InputField from "components/Client/Common/InputField";

export const useHandleRenderFields = (
  currentUser,
  optimisticUpdateUserInfo
) => {
  const [isBothAddressExist, setIsBothAddressExist] = React.useState(false);
  const [isBothContactExist, setIsBothContactExist] = React.useState(false);

  const { usr_address1, usr_address2, usr_mobile, usr_phone } =
    currentUser.user || {};

  const getExistingAddressKey = React.useMemo(() => {
    if (!usr_address1 && !usr_address2) {
      return "usr_address1";
    }

    if (usr_address1 && !usr_address2) {
      return "usr_address1";
    }

    return "usr_address2";
  }, [usr_address1, usr_address2]);

  const getExistingContactKey = React.useMemo(() => {
    if (!usr_mobile && !usr_phone) {
      return "usr_mobile";
    }

    if (usr_mobile && !usr_phone) {
      return "usr_mobile";
    }

    return "usr_phone";
  }, [usr_mobile, usr_phone]);

  const getExistingContactValue = React.useMemo(() => {
    if (!usr_mobile && !usr_phone) {
      return usr_mobile;
    }

    if (usr_mobile && !usr_phone) {
      return usr_mobile;
    }

    return usr_phone;
  }, [usr_mobile, usr_phone]);

  const getExistingAddressValue = React.useMemo(() => {
    if (!usr_address1 && !usr_address2) {
      return usr_address1;
    }

    if (usr_address1 && !usr_address2) {
      return usr_address1;
    }

    return usr_address2;
  }, [usr_address1, usr_address2]);

  const initialValues = {
    [getExistingAddressKey]: getExistingAddressValue || "",
    [getExistingContactKey]: getExistingContactValue || "",
    cash_method: "Cash_on_delivery",
  };

  const formValidation = (values) => {
    let errors = {};

    if (!values[getExistingAddressKey]) {
      errors[getExistingAddressKey] = "Required";
    } else if (values[getExistingAddressKey].length < 10) {
      errors[getExistingAddressKey] = "Address must contain 10 characters";
    }

    if (!values[getExistingContactKey]) {
      errors[getExistingContactKey] = "Required";
    } else if (
      !/^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/i.test(
        values[getExistingContactKey]
      )
    ) {
      errors[getExistingContactKey] = "Please enter valid mobile number";
    }

    return errors;
  };

  const renderAddressFields = (handleChange, values, errors) => {
    if (isBothAddressExist) {
      const addressOptions = [
        { label: usr_address1, value: usr_address1 },
        { label: usr_address2, value: usr_address2 },
      ];

      return (
        <SelectField
          label="Address"
          name={[getExistingAddressKey]}
          onChange={handleChange}
          value={values[getExistingAddressKey]}
          options={addressOptions}
        />
      );
    }
    return (
      <InputField
        label="Permanent Address"
        onBlur={(e) => optimisticUpdateUserInfo(e, values, errors)}
        onChange={handleChange}
        value={values[getExistingAddressKey]}
        name={[getExistingAddressKey]}
        error={errors[getExistingAddressKey]}
        placeholder="Enter Address"
      />
    );
  };

  const renderContactFields = (handleChange, values, errors) => {
    if (isBothContactExist) {
      const contactOptions = [
        { label: usr_mobile, value: usr_mobile },
        { label: usr_phone, value: usr_phone },
      ];

      return (
        <SelectField
          label="Contact#"
          name={[getExistingContactKey]}
          onChange={handleChange}
          value={values[getExistingContactKey]}
          options={contactOptions}
        />
      );
    }
    return (
      <InputField
        label="Contact Number"
        onBlur={(e) => optimisticUpdateUserInfo(e, values, errors)}
        onChange={handleChange}
        value={values[getExistingContactKey]}
        name={[getExistingContactKey]}
        error={errors[getExistingContactKey]}
        placeholder="0333-3333333"
      />
    );
  };

  React.useEffect(() => {
    if (usr_address1?.length && usr_address2?.length) {
      setIsBothAddressExist(true);
    }

    if (usr_mobile?.length && usr_phone?.length) {
      setIsBothContactExist(true);
    }
  }, [usr_address1, usr_address2, usr_mobile, usr_phone]);

  return {
    initialValues,
    formValidation,
    renderAddressFields,
    renderContactFields,
    getExistingAddressKey,
    getExistingContactKey,
  };
};
