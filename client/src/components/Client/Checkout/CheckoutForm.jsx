import React from "react";
import { withRouter } from "react-router-dom";
import { Formik, Form } from "formik";
import Logo from "logo.svg";
import StripeCheckout from "react-stripe-checkout";
import SelectField from "../Common/SelectField";
import { useDispatch, useSelector } from "react-redux";
import { useHandleRenderFields } from "./hooks/useHandleRenderFields";
import { useTotalAmountOfCartProducts } from "../Cart/CartPopUpBoxButton";
import { fetchWrapper } from "services/restApi";
import { updateOptimistically } from "redux/actions/User";
import { checkoutProduct } from "redux/actions/Stripe";
import "./CheckoutForm.css";

const publishKey = "pk_test_JqXsmT3NLptMKQdDKcn0wK0z00QoEHdHtA";
const PaymentDropDownField = [
  { label: "Cash on delivery", value: "Cash_on_delivery" },
  { label: "Card payment", value: "Card_Payment" },
];

const CheckoutForm = ({ history }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.currentUser);
  const { product } = useSelector((state) => state.product);
  const { totalAmount } = useTotalAmountOfCartProducts(product);

  const email = currentUser.isGoogleAuth
    ? currentUser.user.usr_email
    : currentUser.user.usr_email;

  const optimisticUpdateUserInfo = async (event, values, errors) => {
    if (!errors[event.target.name] && values[event.target.name]) {
      dispatch(
        updateOptimistically({ [event.target.name]: event.target.value, email })
      );
    }
  };

  const {
    initialValues,
    formValidation,
    renderAddressFields,
    renderContactFields,
    getExistingAddressKey,
    getExistingContactKey,
  } = useHandleRenderFields(currentUser, optimisticUpdateUserInfo);

  const createPayloadStructureForRequest = (values) => {
    if (!product?.length) return null;

    return {
      ["user_id"]: currentUser.user.usr_id,
      ["order_payment_method"]: values.cash_method,
      ["order_address"]: values[getExistingAddressKey],
      ["order_delivery_status"]: "pending",
      ["order_price"]: totalAmount,
      ["products"]: product,
    };
  };

  const submitFormWithCashOnDelivery = async (values) => {
    try {
      const payload = createPayloadStructureForRequest(values);
      await fetchWrapper("POST", "/api/checkout", payload);
      // dispatch(checkoutProduct());
      history.push("user/orders");
    } catch (error) {}
  };

  const submitFormWithCardPayment = async (token, values) => {
    try {
      const payload = createPayloadStructureForRequest(values);

      await fetchWrapper("POST", "checkout/stripe/pay", {
        ...token,
        ...payload,
      });
      // dispatch(checkoutProduct());
      history.push("user/orders");
    } catch (error) {}
  };

  const handleSubmit = (values) => submitFormWithCashOnDelivery(values);

  const isDisabled = (values, errors) => {
    return (
      Object.values(errors)?.length ||
      values[getExistingAddressKey].length === 0 ||
      values[getExistingContactKey].length === 0
    );
  };

  return (
    <div className="checkout-form-wrapper">
      <h3 className="checkout-form-wrapper__title">Customer Details</h3>
      <Formik initialValues={initialValues} validate={formValidation}>
        {({ values, handleChange, handleBlur, errors }) => (
          <Form>
            {renderAddressFields(handleChange, values, errors)}

            {renderContactFields(handleChange, values, errors)}

            <SelectField
              label="Payment Method"
              name="cash_method"
              onChange={handleChange}
              value={values.cash_method}
              options={PaymentDropDownField}
            />

            {values.cash_method === "Card_Payment" && (
              <StripeCheckout
                email={email}
                label="Pay Now"
                name="Kapra Bazar Ltd"
                image={Logo}
                description={`Your total is $${totalAmount}`}
                amount={totalAmount * 100}
                panelLabel="Pay Now"
                bullingAddress
                shippingAddress
                token={(token) => submitFormWithCardPayment(token, values)}
                stripeKey={publishKey}
              />
            )}

            <button
              type="button"
              onClick={() => handleSubmit(values)}
              disabled={isDisabled(values, errors)}
              className="checkout-btn"
            >
              Proceed to checkout
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default withRouter(CheckoutForm);
