import React from "react";
import { Typography } from "@material-ui/core";
import "./CheckoutDetailList.css";

const CheckoutDetailList = ({ product }) => {
  const getPriceOfProduct = (product) => {
    return product.product_discountprice
      ? product.product_discountprice
      : product.product_originalprice;
  };

  return (
    <div className="checkout-order-info-section__product-item">
      <Typography variant="body1" className="product-item__count">
        1
      </Typography>
      <Typography variant="body1" className="product-item__count-x">
        x
      </Typography>
      <Typography variant="body1" className="product-item__title">
        {product.product_name} | {product.cartProductQuantity} pc(s)
      </Typography>
      <Typography variant="body1" className="product-item__price">
        ${getPriceOfProduct(product)}
      </Typography>
    </div>
  );
};

export default CheckoutDetailList;
