import React from "react";
import { useSelector } from "react-redux";
import CheckoutDetailList from "./CheckoutDetailList";
import { useTotalAmountOfCartProducts } from "../Cart/CartPopUpBoxButton";
import "./CheckoutDetails.css";

const CheckoutDetails = () => {
  const { product } = useSelector((state) => state.product);
  const { totalAmount } = useTotalAmountOfCartProducts(product);

  return (
    <div className="checkout-cart-wrapper">
      <div className="checkout-order-info-section">
        <h3 className="checkout-order-info-section__title">Your Order</h3>
        <div className="checkout-order-info-section__product-wrapper">
          {product.map((item) => (
            <CheckoutDetailList product={item} />
          ))}
        </div>
      </div>
      <hr style={{ color: "white" }} />
      <div className="checkout-cart-calculation-section">
        <div className="checkout-cart-calculation-section__text-wrapper">
          <span>Sub Total</span>
          <span>${totalAmount}</span>
        </div>
        <div className="checkout-cart-calculation-section__text-wrapper">
          <span>Discount</span>
          <span>$0.00</span>
        </div>
        <div className="checkout-cart-calculation-section__text-wrapper font-weight-bold mt-2">
          <span>Total</span>
          <span>${totalAmount}</span>
        </div>
      </div>
    </div>
  );
};

export default CheckoutDetails;
