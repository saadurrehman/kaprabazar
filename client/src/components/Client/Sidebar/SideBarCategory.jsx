import React from "react";
import SideBarContent from "../Sidebar/SideBarContent";
import "./SideBarCategory.css";

const SideBarCategory = ({
  categoryID,
  subCategoryID,
  categories,
  setSubCategoryID,
  setCategoryID,
}) => {
  return (
    <div style={{ padding: "35px 0px 45px", paddingTop: "45px" }}>
      <div className="side-bar-style">
        <div className="side-bar-style__inner">
          <div style={{ paddingLeft: "50px", paddingRight: "20px" }}>
            {categories?.map((item) => (
              <SideBarContent
                categoryID={categoryID}
                subCategoryID={subCategoryID}
                setSubCategoryID={setSubCategoryID}
                setCategoryID={setCategoryID}
                subCategory={item.sub_categories}
                productCategory={item}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideBarCategory;
