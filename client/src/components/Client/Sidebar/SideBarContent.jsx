import React from "react";
import Slide from "@material-ui/core/Slide";
import "./SideBarContent.css";

const SideBarContent = ({
  subCategoryID,
  categoryID,
  subCategory = [],
  setSubCategoryID,
  setCategoryID,
  productCategory,
}) => {
  const [isOpenDropdown, setIsOpenDropdown] = React.useState(true);

  const handleSubCategory = (subCategoryID) => {
    setSubCategoryID(subCategoryID);
    setCategoryID(productCategory["product_category_id"]);
  };

  const getIsActiveCategoryTab = (subcategory_id) => {
    return (
      productCategory?.product_category_id === categoryID &&
      subcategory_id === subCategoryID
    );
  };

  return (
    <div className="side-bar__content">
      <header
        className="side-bar__header"
        onClick={() => setIsOpenDropdown(!isOpenDropdown)}
      >
        <div>
          <img
            src={productCategory?.product_category_icon_url}
            className="side-bar-icon"
            width="23"
            height="23"
          />
        </div>
        <span className="side-bar__title">
          {productCategory?.product_category_name}
        </span>
        {subCategory?.length > 0 && (
          <button
            className={
              "side-bar__dropdown-btn-arrow-right" +
              (isOpenDropdown ? " side-bar__dropdown-btn-arrow-down" : "")
            }
          >
            <i className="fa fa-chevron-down"></i>
          </button>
        )}
      </header>
      {subCategory?.length > 0 && isOpenDropdown && (
        <div className="subCategory-wrapper">
          {subCategory.map((item, index) => (
            <Slide
              key={index}
              direction="down"
              in={isOpenDropdown}
              mountOnEnter
              unmountOnExit
            >
              <div
                key={item.subcategory_id}
                onClick={() => handleSubCategory(item.subcategory_id)}
                className="subCategory-header"
              >
                <header>
                  <span
                    className={`subCategory__title ${
                      getIsActiveCategoryTab(item.subcategory_id)
                        ? "subCategory__title-active"
                        : ""
                    }`}
                  >
                    {item.subcategory_name}
                  </span>
                </header>
              </div>
            </Slide>
          ))}
        </div>
      )}
    </div>
  );
};

export default SideBarContent;
