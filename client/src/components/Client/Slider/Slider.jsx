import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import img1 from "../../../images/Slider_images/3.PNG";
import img2 from "../../../images/Slider_images/1.PNG";
import img3 from "../../../images/Slider_images/2.PNG";
import "./slider.css";

const Slider = () => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const CustomRightArrow = ({ onClick, ...rest }) => {
    return (
      <button className="slider__rightArrow" onClick={() => onClick()}>
        <i className="fa fa-arrow-left"></i>
      </button>
    );
  };
  const CustomLeftArrow = ({ onClick, ...rest }) => {
    return (
      <button className="slider__leftArrow" onClick={() => onClick()}>
        <i className="fa fa-arrow-right"></i>
      </button>
    );
  };

  return (
    <div className="carouselPadding">
      <Carousel
        swipeable={true}
        draggable={true}
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        infinite={true}
        autoPlay={false}
        autoPlaySpeed={1000}
        keyBoardControl={true}
        // customTransition="all .5"
        transitionDuration={500}
        slidesToSlide={1}
        containerClass="carousel-container"
        //   deviceType={this.props.deviceType}
        itemClass="carousel-item-padding-40-px"
        customRightArrow={<CustomLeftArrow />}
        customLeftArrow={<CustomRightArrow />}
      >
        <div>
          <img
            src={img1}
            alt="slider 1"
            style={{ padding: "1rem", width: "100%" }}
          />
        </div>
        <div>
          {" "}
          <img
            src={img2}
            alt="slider 1"
            style={{ padding: "1rem", width: "100%" }}
          />
        </div>
        <div>
          {" "}
          <img
            src={img3}
            alt="slider 1"
            style={{ padding: "1rem", width: "100%" }}
          />
        </div>
      </Carousel>
    </div>
  );
};

export default Slider;
