import React from "react";
import { Formik, Form } from "formik";
import { Link } from "react-router-dom";
import Modal from "@material-ui/core/Modal";
import { useDispatch } from "react-redux";
import InputField from "../Common/InputField";
import { useNavbarDispatch } from "contextApi/navbarContextapi";
import { registerUser, loginUser } from "../../../redux/actions/User";
import "./signup-signin.css";

const BACKEND_URL = "https://kapra-bazar.herokuapp.com";
const initialValues = {
  username: "",
  email: "",
  password: "",
};
const emailRegex =
  /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(?!gmail)(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
const RenderFormHeader = ({ heading, subHeading }) => {
  return (
    <>
      <h3 className="auth-form-container__heading">{heading}</h3>
      <span className="auth-form-container__Subheading">{subHeading}</span>
    </>
  );
};

const RenderSocialButton = ({ onClick, icon, buttonName }) => {
  const bgColor =
    icon === "facebook" ? "rgb(66, 103, 178)" : "rgb(66, 133, 244)";

  return (
    <button
      className="input-form__continuebtn"
      onClick={onClick}
      style={{
        backgroundColor: bgColor,
        marginBottom: "10px",
      }}
    >
      <i className={`fa fa-${icon} mr-2`}></i>
      {buttonName}
    </button>
  );
};

const formValidation = (values, isLoginPage, isSignUpPage) => {
  let errors = {};

  if (isSignUpPage) {
    if (!values.username) errors.username = "Required";
    else if (values.username.length < 4)
      errors.username = "Username atleast contain 4 character";
  }
  if (isSignUpPage || isLoginPage) {
    if (!values.email) errors.username = "Required";
    else if (!emailRegex.test(values.email)) errors.email = "SignIn With Gmail";

    if (!values.password) errors.username = "Required";
    else if (values.password.length < 8)
      errors.password = "8 characters required";

    return errors;
  } else {
    if (!values.email) errors.username = "Required";
    else if (!emailRegex.test(values.email)) errors.email = "SignIn With Gmail";
  }
  return errors;
};

function SignupSignInModal({ openLoginModal, handleCloseLoginModal }) {
  const [isLoginPage, setIsLoginPage] = React.useState(true);
  const [isSignUpPage, setIsSignUpPage] = React.useState(false);
  const [isResetPasswordPage, setIsResetPasswordPage] = React.useState(false);
  const { setZIndex } = useNavbarDispatch();
  const dispatch = useDispatch();

  const handleSubmit = async (values) => {
    if (isSignUpPage) {
      const response = await registerUser({
        email: values.email,
        password: values.password,
        username: values.username,
      })(dispatch);

      if (response) {
        handleCloseLoginModal();
        setZIndex(9999);
      }

      return;
    }

    if (isLoginPage) {
      const response = await loginUser({
        email: values.email,
        password: values.password,
      })(dispatch);

      if (response) {
        handleCloseLoginModal();
      }

      return;
    }
  };

  const signInWithGoogle = async () => {
    window.location = `${BACKEND_URL}/google`;
    handleCloseLoginModal();
  };
  const signInWithFacebook = async () => {
    window.location = `${BACKEND_URL}/facebook`;
    handleCloseLoginModal();
  };

  const RenderLinks = ({
    label,
    buttonName,
    loginPage,
    resetPasswordPage,
    signUpPage,
  }) => {
    return (
      <p className="input-form__SignupText">
        {label}
        <Link
          to=""
          onClick={() => {
            setIsLoginPage(loginPage);
            setIsResetPasswordPage(resetPasswordPage);
            setIsSignUpPage(signUpPage);
          }}
          className="input-form__Signupbtn"
        >
          {buttonName}
        </Link>
      </p>
    );
  };

  const isDisabled = (errors, values) => {
    if (isSignUpPage) {
      return (
        !!Object.values(errors).length ||
        values.email.length === 0 ||
        values.password.length === 0 ||
        values.username.length === 0
      );
    }

    if (isLoginPage) {
      return (
        !!Object.values(errors).length ||
        values.email.length === 0 ||
        values.password.length === 0
      );
    }

    return !!Object.values(errors).length || values.email.length === 0;
  };

  return (
    <div>
      <Modal
        open={openLoginModal}
        onClose={() => {
          handleCloseLoginModal();
          setZIndex(9999);
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <>
          <button
            className="simple-modal__close-btn fa fa-close"
            onClick={() => {
              handleCloseLoginModal();
              setZIndex(9999);
            }}
          ></button>
          <div className="simple-modal__wrapper">
            <div className="simple-modal__holder-wrapper">
              <div className="simple-modal__holder">
                <div className="simple-modal__auth-form">
                  <div className="simple-modal__auth-form-container">
                    <Formik
                      onSubmit={(values) => handleSubmit(values)}
                      initialValues={initialValues}
                      validate={(values) =>
                        formValidation(values, isLoginPage, isSignUpPage)
                      }
                    >
                      {({ values, handleChange, errors }) => (
                        <Form>
                          {isLoginPage && (
                            <RenderFormHeader
                              heading="Welcome Back"
                              subHeading=" Login with your email & password"
                            />
                          )}
                          {isSignUpPage && (
                            <RenderFormHeader
                              heading="Sign Up"
                              subHeading="By signing up, you agree to Pickbazar's"
                            />
                          )}
                          {isResetPasswordPage && (
                            <RenderFormHeader
                              heading="Forgot Password"
                              subHeading=" We'll send you a link to reset your password"
                            />
                          )}

                          {isSignUpPage && (
                            <InputField
                              onChange={handleChange}
                              value={values.username}
                              name="username"
                              inputClassName="input-form__emailField"
                              error={errors.username}
                              errorClassName="error"
                              parentClassName="none"
                              placeholder="Enter Username"
                            />
                          )}
                          <InputField
                            onChange={handleChange}
                            value={values.email}
                            name="email"
                            inputClassName="input-form__emailField"
                            error={errors.email}
                            errorClassName="error"
                            parentClassName="none"
                            placeholder="demo@demo.com"
                          />

                          {(isLoginPage || isSignUpPage) && (
                            <InputField
                              onChange={handleChange}
                              value={values.password}
                              name="password"
                              type="password"
                              parentClassName="none"
                              inputClassName="input-form__passwordField"
                              placeholder="Password"
                              error={errors.password}
                              errorClassName="error"
                            />
                          )}
                          <button
                            type="submit"
                            disabled={isDisabled(errors, values)}
                            className="input-form__continuebtn"
                          >
                            {isResetPasswordPage ? "Reset" : "Continue"}
                          </button>
                        </Form>
                      )}
                    </Formik>
                    {(isLoginPage || isSignUpPage) && (
                      <>
                        <div className="auth-form-container___divider">
                          <span>Or</span>
                        </div>
                        {/* <RenderSocialButton
                          onClick={signInWithFacebook}
                          icon="facebook"
                          buttonName="Continue With Facebook"
                        /> */}
                        <RenderSocialButton
                          onClick={signInWithGoogle}
                          icon="google"
                          buttonName="Continue With Google"
                        />
                      </>
                    )}
                    {isLoginPage && (
                      <RenderLinks
                        label="Don't have any account?"
                        buttonName=" Sign UP"
                        loginPage={false}
                        resetPasswordPage={false}
                        signUpPage={true}
                      />
                    )}
                    {isResetPasswordPage && (
                      <RenderLinks
                        label="Back to "
                        buttonName="Login"
                        loginPage={true}
                        resetPasswordPage={false}
                        signUpPage={false}
                      />
                    )}
                    {isSignUpPage && (
                      <RenderLinks
                        label="Already have an account? "
                        buttonName="Login"
                        loginPage={true}
                        resetPasswordPage={false}
                        signUpPage={false}
                      />
                    )}
                  </div>

                  {isLoginPage && (
                    <RenderLinks
                      label="Forgot your password? "
                      buttonName="Reset It"
                      loginPage={false}
                      resetPasswordPage={true}
                      signUpPage={false}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </>
      </Modal>
    </div>
  );
}
export default SignupSignInModal;
