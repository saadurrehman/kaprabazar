import React from "react";
import Modal from "@material-ui/core/Modal";
import TokenField from "../Common/TokenField";

function TwoFactorAuth({ setZIndex, currentUser }) {
  const [showOTPNumber, setShowOTPNumber] = React.useState(true);

  React.useEffect(() => {
    setZIndex(-9999);
  }, []);

  return (
    <div>
      <Modal
        open={showOTPNumber}
        onClose={() => setShowOTPNumber(false)}
        aria-labelledby="example-modal-sizes-title-sm"
        centered
      >
        <div>
          <div className="simple-modal__wrapper">
            <div className="simple-modal__holder-wrapper">
              <div className="simple-modal__holder">
                <div className="simple-modal__auth-form">
                  <div className="simple-modal__auth-form-container">
                    <h2 id="modal-title">Enter 6 digit code</h2>
                    <hr />
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        minHeight: "100px",
                      }}
                    >
                      <TokenField
                        setShowOTPNumber={setShowOTPNumber}
                        setZIndex={setZIndex}
                        email={currentUser.user.usr_email}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default TwoFactorAuth;
