import React from "react";
import Modal from "react-bootstrap/Modal";
import { Row, Button, Form } from "react-bootstrap";
import EditIcon from "@material-ui/icons/Edit";
import { useDispatch } from "react-redux";
import { updateOptimistically } from "redux/actions/User";
import "./ContactCard.css";

const Card = ({ currentUser, title, fieldName, editTitle }) => {
  const email = currentUser.isGoogleAuth
    ? currentUser.user.email
    : currentUser.usr_email;

  const dispatch = useDispatch();

  const currentValue = currentUser.isGoogleAuth
    ? currentUser.user[fieldName]
    : currentUser[fieldName];

  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [field, setField] = React.useState(currentValue ?? "");

  const handleSubmit = () => {
    dispatch(updateOptimistically({ [fieldName]: field, email }));
    setIsModalOpen(false);
  };

  return (
    <div className="contactCard__container">
      <label className="contactCard-container__label">
        <input
          type="radio"
          name="contact"
          class="card-input-element d-none"
          id="contact"
          value={field}
        />
        <div
          class="card card-body bg-light d-flex card-min-height"
          onClick={() => {
            setIsModalOpen(true);
            setField(currentUser.user[fieldName] || "");
          }}
        >
          <Row>
            <span className="card-title">{title}</span>
          </Row>
          <Row>
            <span className="card-content">
              {currentUser.user[fieldName] || ""}
            </span>
          </Row>
          <span className="action-buttonGroup">
            <span className="contact-card__action-button button-edit">
              <EditIcon style={{ fontSize: 14 }} />
            </span>
          </span>
        </div>
      </label>
      {isModalOpen ? (
        <Modal
          size="sm"
          show={isModalOpen}
          onHide={() => setIsModalOpen(false)}
          aria-labelledby="example-modal-sizes-title-sm"
          centered
        >
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicContent">
                {!editTitle ? <Form.Label>{title}</Form.Label> : null}
                <Form.Control
                  type="text"
                  placeholder="Enter your Info"
                  onChange={(e) => setField(e.target.value)}
                  value={field}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setIsModalOpen(false)}>
              Close
            </Button>
            <Button onClick={handleSubmit} variant="success" type="submit">
              Save changes
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
    </div>
  );
};

export default Card;
