import React from "react";
import GIF from "images/loadingGIF/Spinner-1s-200px.gif";

function LoadImage({ imgClass, url, alt = "", ...rest }) {
  const [isImageLoaded, setIsImageLoaded] = React.useState(false);
  return (
    <div>
      {isImageLoaded ? (
        <img
          className={imgClass ? imgClass : ""}
          src={url}
          alt={alt}
          onLoad={() => setIsImageLoaded(true)}
          {...rest}
        />
      ) : (
        <img
          className={imgClass ? imgClass : ""}
          src={GIF}
          alt={alt}
          onLoad={() => setIsImageLoaded(true)}
          {...rest}
        />
      )}
    </div>
  );
}

export default LoadImage;
