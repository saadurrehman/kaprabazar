import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
function GlobalLoader() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "50vh",
        width: "100%",
      }}
    >
      <CircularProgress />
    </div>
  );
}

export default GlobalLoader;
