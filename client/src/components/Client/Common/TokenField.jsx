import React from "react";
import toastr from "toastr";
import { useDispatch } from "react-redux";
import { twoFactorAuthenticationStatus } from "redux/actions/User";
import { fetchWrapper } from "services/restApi";

const TOKEN_LENGTH = 6;
const numberRegex = /^[0-9]*$/;

function TokenField({ email, setShowOTPNumber, setZIndex }) {
  const [val, setVal] = React.useState("");
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (val.length === TOKEN_LENGTH) {
      const verifyOTP = async () => {
        try {
          const { validated } = await fetchWrapper(
            "POST",
            "http://localhost:5555/api/user/verify/otp",
            {
              email,
              token: val,
            }
          );

          if (validated) {
            dispatch(twoFactorAuthenticationStatus());
            setShowOTPNumber(false);
            setZIndex(9999);
            toastr.success("User is Authenticated");
            return;
          }

          toastr.error("Invalid Code, Please try again");
        } catch (error) {
          setShowOTPNumber(false);
          setZIndex(9999);
        }
      };
      verifyOTP();
    }
  }, [val, dispatch, setShowOTPNumber, email, setZIndex]);

  return (
    <div>
      <input
        type="text"
        onChange={(e) => {
          if (numberRegex.test(e.target.value)) setVal(e.target.value);
        }}
        className="block"
        value={val}
        maxlength="6"
        autoFocus
      />
    </div>
  );
}

export default TokenField;
