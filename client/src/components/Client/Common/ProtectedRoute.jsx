import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";
// import { getCurrentUser } from "services/authService";
const ProtectedRoute = ({ component: Component, render, ...rest }) => {
  const currentUser = useSelector((state) => state.currentUser);

  return (
    <Route
      {...rest}
      component={(props) => {
        if (
          !currentUser ||
          (currentUser?.user?.is_otp_active === 1 &&
            !currentUser?.isLoggedWithOTP)
        )
          return (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location },
              }}
            />
          );

        return <Component {...props} {...rest} />;
      }}
    />
  );
};

export default ProtectedRoute;
