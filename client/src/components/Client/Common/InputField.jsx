import React from "react";

function InputField({
  type = "text",
  label,
  onBlur = null,
  onChange = null,
  name,
  value,
  error,
  parentClassName = "form-group",
  errorClassName = "form-text text-muted",
  inputClassName = "form-control",
  placeholder,
}) {
  return (
    <div className={parentClassName}>
      {label && <label>{label}</label>}
      <input
        type={type}
        className={inputClassName}
        onBlur={onBlur}
        onChange={onChange}
        value={value}
        name={name}
        placeholder={placeholder}
      />
      <small className={errorClassName}> {error}</small>
    </div>
  );
}

export default InputField;
