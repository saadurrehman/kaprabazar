import React from "react";
import Navbar from "../Navbar/Navbar";

const WrapNavbarWithChildren = ({
  zIndex,
  handleOpenSearchModal,
  openLoginModal,
  handleOpenLoginModal,
  handleCloseLoginModal,
  Children,
}) => {
  return (
    <>
      <Navbar
        zIndex={zIndex}
        handleOpenSearchModal={handleOpenSearchModal}
        openLoginModal={openLoginModal}
        handleOpenLoginModal={handleOpenLoginModal}
        handleCloseLoginModal={handleCloseLoginModal}
      />
      {Children}
    </>
  );
};

export default WrapNavbarWithChildren;
