import React from "react";

function SelectField({ label, name, onChange, value, options }) {
  return (
    <div className="form-group mt-3">
      <label>{label}</label>
      <br />
      <select
        name={name}
        onChange={onChange}
        value={value}
        className="form-control"
      >
        {options.map((item) => (
          <option value={item.value} label={item.label} />
        ))}
      </select>
    </div>
  );
}

export default SelectField;
