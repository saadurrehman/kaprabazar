import React from "react";
import { Link } from "react-router-dom";
import { logoutUser } from "../../../redux/actions/User";
import { useDispatch, useSelector } from "react-redux";
import "./NavbarPopover.css";

const NavbarPopover = () => {
  const dispatch = useDispatch();
  const { product } = useSelector((state) => state.product);

  return (
    <div className="navbar-popover-content">
      <div className="popover-content__inner-wrap">
        <div className="popover-content__menu-item">
          <Link to="/user/profile">Profile</Link>
        </div>
        {product.length > 0 && (
          <div className="popover-content__menu-item">
            <Link to="/checkout">Checkout</Link>
          </div>
        )}
        <div className="popover-content__menu-item">
          <Link to="/user/orders">Order</Link>
        </div>
        <div className="popover-content__menu-item">
          <Link to="" onClick={() => dispatch(logoutUser())}>
            Logout
          </Link>
        </div>
      </div>
    </div>
  );
};
export default NavbarPopover;
