import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import SignupSignInModal from "../signup-singin/SignupSignInModal";
import NavbarPopover from "../Navbar-Popover/NavbarPopover";
import { useNavbar } from "contextApi/navbarContextapi";
import { useDialog } from "hooks/useDialog";
import "./Navbar.css";

const Navbar = ({ handleOpenSearchModal }) => {
  const [isPopoverOpen, setIsPopoverOpen] = React.useState(false);
  const [isOpen, { openDialog, closeDialog }] = useDialog();
  const user = useSelector((state) => state.currentUser);
  const [zIndex, { setZIndex }] = useNavbar();

  const handleOpenLoginModal = () => {
    setZIndex(-9999);
    openDialog();
  };

  const handleCloseLoginModal = () => {
    setZIndex(9999);
    closeDialog();
  };

  const getProfilePicture = () => {
    if (user?.isFacebookAuth) {
      return user.profile.picture.data.url;
    }

    if (user?.isGoogleAuth) {
      return user.profile.picture;
    }

    return "";
  };

  return (
    <>
      <nav
        className="navbar navbar-expand-lg navbar-light"
        style={{ zIndex: `${zIndex}` }}
      >
        <div>
          {/* <img
            width="90"
            height="70"
            src={`${process.env.PUBLIC_URL}/logo.png`}
          /> */}
          <span className="navbar-logo-first-half">KAPRA</span>
          <span className="navbar-logo-second-half">BAZAR</span>
        </div>
        <button
          className="navbar-toggler navbar__togglebtn"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <NavLink
              to="/"
              activeClassName="navItem___active"
              className="nav-link"
            >
              Home
            </NavLink>

            <a className="nav-link navItem___active" href="#products">
              Products
            </a>

            {!user ? (
              <Button
                color="primary"
                onClick={handleOpenLoginModal}
                className="navbar-nav navbar-nav__joinbtn"
              >
                Login
              </Button>
            ) : (
              <div
                className="nav-link nav-avatar"
                onClick={() => setIsPopoverOpen(!isPopoverOpen)}
              >
                <Avatar src={getProfilePicture()} />
                {isPopoverOpen && <NavbarPopover />}
              </div>
            )}
          </ul>

          {/* Login Modal */}
          <SignupSignInModal
            openLoginModal={isOpen}
            handleCloseLoginModal={handleCloseLoginModal}
          />
        </div>

        {/* handling Mobile view for avatar and search button*/}
        <NavLink
          to="/"
          onClick={handleOpenSearchModal}
          className={"search-btn " + (user ? "search-btn__nav-avatar" : "")}
        >
          <i className="fa fa-search"></i>
        </NavLink>
        {user && (
          <div
            className="nav-link nav-avatar-on-Collapse"
            onClick={() => setIsPopoverOpen(!isPopoverOpen)}
          >
            <Avatar src={getProfilePicture()} />
            {isPopoverOpen && <NavbarPopover />}
          </div>
        )}
      </nav>
    </>
  );
};

export default Navbar;
