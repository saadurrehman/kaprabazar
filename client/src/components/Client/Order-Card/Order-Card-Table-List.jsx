import React from "react";
import LoadImage from "../Common/LoadImage";

const OrderCardTableList = ({ imageUrl, item, quantity, price }) => {
  return (
    <tr>
      <td>
        <LoadImage width="30" height="30" url={imageUrl} />
      </td>
      <td>{item}</td>
      <td>{quantity}</td>
      <td>${price}</td>
    </tr>
  );
};

export default OrderCardTableList;
