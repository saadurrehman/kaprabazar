import React from "react";
import moment from "moment";
import OrderCardDetailModal from "./Order-Card-Detail-Modal";
import { useDialog } from "../../../hooks/useDialog";
import "./Order-Card.css";

const OrderCard = ({ item }) => {
  const [isOpen, { openDialog, closeDialog }] = useDialog();
  return (
    <>
      <div className="order-card-container" onClick={() => openDialog()}>
        <div className="order-card-header">
          <span>Order # 1</span>
          <span className="order-card-status">Order On the Way</span>
        </div>
        <div className="order-card-body__wrapper">
          <div className="order-card-body__container">
            <span>Order Date</span>
            <span>{moment(item.order_created_at).format("DD-MM-YYYY")}</span>
          </div>
          <div className="order-card-body__container">
            <span>Delivery Status</span>
            <span>${item.order_delivery_status}</span>
          </div>
          <div className="order-card-body__container">
            <span>Total amount</span>
            <span>${item.order_price}</span>
          </div>
        </div>
      </div>
      {isOpen && (
        <OrderCardDetailModal
          item={item}
          openModal={openDialog}
          closeModal={closeDialog}
        />
      )}
    </>
  );
};

export default OrderCard;
