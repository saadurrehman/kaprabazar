import React from "react";
import OrderCardDetailTable from "../Order-Card/Order-Card-Detail-Table";
import { Modal, Button } from "react-bootstrap";
import "./Order-Card-Detail-Modal.css";

const OrderCardDetailModal = ({ item, openModal, closeModal }) => {
  return (
    <Modal
      size="lg"
      show={() => openModal()}
      onHide={() => closeModal()}
      aria-labelledby="example-modal-sizes-title-sm"
      centered
    >
      <Modal.Header closeButton>Order Detail</Modal.Header>
      <Modal.Body>
        <div>
          <section className="order-detail-wrapper">
            <div className="order-detail__delivery-address">
              <h4>Delivery Address</h4>
              <span>{item.order_address}</span>
            </div>
            <div className="order-detail__charges-detail">
              <div className="charges-detail-wrapper">
                <span>SubTotal</span>
                <span>${item.order_price}</span>
              </div>
              <div className="charges-detail-wrapper charges-detail-wrapper__total-amount">
                <span>Total</span>
                <span>${item.order_price}</span>
              </div>
            </div>
          </section>
          <section>
            <OrderCardDetailTable item={item} />
          </section>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={() => closeModal()}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default OrderCardDetailModal;
