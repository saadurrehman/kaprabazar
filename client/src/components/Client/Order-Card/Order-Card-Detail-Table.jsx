import React from "react";
import { Table } from "react-bootstrap";
import OrderCardTableList from "./Order-Card-Table-List";
const OrderCardDetailTable = ({ item }) => {
  const getPriceOfProduct = (product) => {
    return product.product_discountprice
      ? product.product_discountprice
      : product.product_originalprice;
  };

  return (
    <Table hover>
      <thead>
        <tr>
          <th></th>
          <th>Item</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {item.order_product_details.map((orderProduct) => (
          <OrderCardTableList
            imageUrl={orderProduct.product.product_images[0].product_image_url}
            item={orderProduct.product.product_name}
            quantity={orderProduct.product_quantity}
            price={
              getPriceOfProduct(orderProduct.product) *
              orderProduct.product_quantity
            }
          />
        ))}
      </tbody>
    </Table>
  );
};

export default OrderCardDetailTable;
