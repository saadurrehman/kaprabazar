import React from "react";
import Modal from "@material-ui/core/Modal";
import SearchForm from "../SearchForm/SearchForm";
import "./SearchMobileView.css";

const SearchMobileView = ({ openSearchModel, handleCloseSearchModal }) => {
  return (
    <div>
      <Modal
        open={openSearchModel}
        onClose={handleCloseSearchModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className="search-bar__wrapper">
          <button
            onClick={handleCloseSearchModal}
            className="search-bar__backbtn"
          >
            <i className="fa fa-arrow-left"></i>
          </button>
          <SearchForm
            formClassnames="search-bar__form"
            inputClassnames="search-bar-form__inputfield"
            boxClassnames="search-bar-form__box"
            btnClassnames="search-bar-form__searchbtn"
            iconClassnames="fa fa-search"
          />
        </div>
      </Modal>
    </div>
  );
};

export default SearchMobileView;
