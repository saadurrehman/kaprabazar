import React from "react";
import SideBarCategory from "../Sidebar/SideBarCategory";
import GlobalLoader from "../Common/GlobalLoader";
import ResponsiveCategoryButton from "../Category/ResponsiveCategoryButton";
import ProductsSection from "../Product/ProductsSection";
import Sticky from "wil-react-sticky";
import { useFetch } from "hooks/useFetch";
import "./maincontent.css";

const MainContent = () => {
  const { data, isLoading, setUrl } = useFetch();
  const [categoryID, setCategoryID] = React.useState(2);
  const [subCategoryID, setSubCategoryID] = React.useState(1);

  React.useEffect(() => {
    async function getCategories() {
      setUrl("/api/product/category");
    }

    getCategories();
  }, [setUrl]);

  return (
    <>
      {/* Responsive Mobile screen category starts*/}
      <div className="popover-wrapper">
        <ResponsiveCategoryButton
          isLoading={isLoading}
          categories={data?.categories}
        />
      </div>
      {/* Responsive Mobile screen category ends*/}

      <div className="main-content">
        <div id="containerSelectorFocus" className="side-bar-category">
          <Sticky
            containerSelectorFocus="#containerSelectorFocus"
            offsetTop={20}
          >
            <div className="side-bar-category__wrapper">
              {isLoading && <GlobalLoader />}

              <SideBarCategory
                categoryID={categoryID}
                subCategoryID={subCategoryID}
                setSubCategoryID={setSubCategoryID}
                setCategoryID={setCategoryID}
                categories={data?.categories}
              />
            </div>
          </Sticky>
        </div>

        <ProductsSection
          categoryID={categoryID}
          subCategoryID={subCategoryID}
        />
      </div>
    </>
  );
};

export default MainContent;
