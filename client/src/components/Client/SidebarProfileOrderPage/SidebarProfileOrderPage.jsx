import React from "react";
import { NavLink } from "react-router-dom";
import { Col } from "react-bootstrap";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import "./SidebarProfileOrderPage.css";

const SidebarProfileOrderPage = () => {
  const [isPopoverOpen, setIsPopoverOpen] = React.useState(false);
  return (
    <>
      <Col className="d-none d-lg-block" lg={3}>
        <div className="sidebar">
          <div className="sidebar__upperSection">
            <NavLink
              to="/user/orders"
              className="sidebar__upperSection--item"
              activeClassName="sidebar__activeItem"
            >
              Your Order
            </NavLink>
            <NavLink
              to="/user/help"
              className="sidebar__upperSection--item"
              activeClassName="sidebar__activeItem"
            >
              Need Help
            </NavLink>
            <NavLink
              to="/user/two-factor-auth"
              className="sidebar__upperSection--item"
              activeClassName="sidebar__activeItem"
            >
              Two factor authentication
            </NavLink>
          </div>
          <div className="sidebar__lowerSection">
            <NavLink
              to="/user/profile"
              className="sidebar__lowerSection--item"
              activeClassName="profile__sidebar__activeItem"
            >
              Profile
            </NavLink>
            <NavLink className="sidebar__lowerSection--item" to="/logout">
              Logout
            </NavLink>
          </div>
        </div>
      </Col>

      {/* For mobile view */}
      <Col className="sidebar-btn">
        <DragIndicatorIcon
          className="sidebar-popover__icon"
          onClick={() => setIsPopoverOpen(!isPopoverOpen)}
        />
        {isPopoverOpen && (
          <div className="popover-card">
            <NavLink
              className="popover-card__link"
              activeClassName="popover-card__activeLink"
              to="/user/orders"
            >
              Your Order
            </NavLink>
            <NavLink
              className="popover-card__link"
              activeClassName="popover-card__activeLink"
              to="/user/help"
            >
              Need Help
            </NavLink>
            <NavLink
              className="popover-card__link"
              activeClassName="popover-card__activeLink"
              to="/user/profile"
            >
              Profile
            </NavLink>
            <NavLink
              className="popover-card__link"
              activeClassName="popover-card__activeLink"
              to="/logout"
            >
              Logout
            </NavLink>
          </div>
        )}
      </Col>
    </>
  );
};

export default SidebarProfileOrderPage;
