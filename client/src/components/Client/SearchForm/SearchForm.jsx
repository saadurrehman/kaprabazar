import React from "react";

const SearchForm = ({
  formClassnames,
  boxClassnames,
  inputClassnames,
  btnClassnames,
  iconClassnames,
  searchBtnText,
}) => {
  return (
    <>
      <form className={formClassnames}>
        <span className={boxClassnames}>Clothing</span>
        <input
          type="search"
          className={inputClassnames}
          placeholder="Search your products from here"
          required
        />
        <button type="button" className={btnClassnames}>
          <i className={iconClassnames}></i>
          {searchBtnText}
        </button>
      </form>
    </>
  );
};

export default SearchForm;
