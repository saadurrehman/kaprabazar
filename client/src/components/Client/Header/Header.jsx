import React from "react";
import "./header.css";
import SearchMobileView from "../Search/SearchMobileView";
import SearchForm from "../SearchForm/SearchForm";

const Header = ({
  handleOpenSearchModal,
  handleCloseSearchModal,
  openSearchModel,
}) => {
  const getSearchResults = () => {
    // Search code
  };
  return (
    <>
      <div className="banner-container">
        <div
          className="header__backgroundimg"
          style={{
            backgroundImage: `url(${process.env.PUBLIC_URL}/IMGS/2.jpg)`,
          }}
        />
        <div className="header__title-container">
          <h2 className="header__heading">Shop your designer dresses</h2>
          <p className="header__substitle">
            Ready to wear dresses tailored for you from online. Hurry up while
            stock lasts.
          </p>
          <div className="header__form-container">
            <SearchForm
              inputClassnames="input-group__searchbar"
              boxClassnames="input-group__searcharbox d-none d-md-block"
              btnClassnames="submit-btn"
              iconClassnames="fa fa-search search-icon"
              searchBtnText="Search"
              formClassnames="form-inline"
            />
          </div>
          <SearchMobileView
            openSearchModel={openSearchModel}
            handleOpenSearchModal={handleOpenSearchModal}
            handleCloseSearchModal={handleCloseSearchModal}
          />
        </div>
      </div>
    </>
  );
};

export default Header;
