const bcrypt = require("bcryptjs");
const passport = require("passport");
const nodemailer = require("nodemailer");
const speakeasy = require("speakeasy");
require("dotenv").config();

const smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: process.env.NODEMAIL_EMAIL,
    pass: process.env.NODEMAIL_PASSWORD,
  },
});

const sendEmailWithVerifyLink = (host, email, userExist) => {
  const link =
    "http://" +
    host +
    "/api/user/register/verify?id=" +
    userExist.usr_hash +
    "&&email=" +
    email;

  const mailOptions = {
    from: "saadurrehman59@gmail.com",
    to: email,
    subject: "Please confirm your Email account",
    html:
      "Hello,<br> Please Click on the link to verify your email.<br><a href=" +
      link +
      ">Click here to verify</a>",
  };
  console.log(mailOptions);
  return { mailOptions };
};

/**
 * @des register User
 * @param {email} req - contains email of the user
 * @param {password} req - contains password of the user
 * @param {*} res - response object
 */
const registerUser = (models) => (req, res, next) => {
  const { users } = models;
  let { username, email, password } = req.body;

  users.findOne({ where: { usr_email: email } }).then((userExist) => {
    if (userExist && userExist.is_usr_active) {
      return res.status(400).json({
        message: "User already exists",
      });
    }

    if (userExist && !userExist.is_usr_active) {
      const { mailOptions } = sendEmailWithVerifyLink(
        req.get("host"),
        email,
        userExist
      );

      smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
          console.log(error);
          res.end("error");
        } else {
          console.log("Message sent: " + response.message);
          res.json({ message: "Please Check your mail" });
        }
      });
      return res.status(200).json({
        message: "Please Check your mail",
      });
    }

    const rand = Math.floor(Math.random() * 100 + 54);
    //hash password
    bcrypt.genSalt(10, (err, salt) =>
      bcrypt.hash(password, salt, (err, hash) => {
        if (err) throw err;

        // set password to hashed
        let Hashpassword = hash;

        //create user
        users
          .create({
            usr_email: email,
            usr_password: Hashpassword,
            usr_name: username,
            usr_hash: rand,
            is_usr_active: 0,
            is_otp_active: 0,
            usr_otp_secret: "null",
          })
          .then((userCreated) => {
            if (userCreated) {
              const { mailOptions } = sendEmailWithVerifyLink(
                req.get("host"),
                email,
                userCreated
              );
              smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                  console.log(error);
                  res.end("error");
                } else {
                  console.log("Message sent: " + response.message);
                  res.json({ message: "Please Check your mail" });
                }
              });
            }
          });
      })
    );
  });
};

const verifyUser = (models) => (req, res, next) => {
  const { id, email } = req.query;

  models.users
    .findOne({ where: { usr_email: email } })
    .then((isUserFound) => {
      if (isUserFound) {
        if (isUserFound.usr_hash == id) {
          req.query.password = isUserFound.usr_email;
          isUserFound
            .update({ is_usr_active: 1, usr_hash: null })
            .then((Userupdated) => {
              if (Userupdated) {
                // add user directly into session
                passport.authenticate("verify", {
                  failureRedirect: "/api/user/getuser",
                  successRedirect:
                    "https://kapra-bazar.herokuapp.com/api/user/getuser",
                })(req, res, next);
              } else {
                console.log("no user updated");
              }
            });
        } else {
          console.log("no id matched");
        }
      } else {
        console.log("no user found");
      }
    })

    .catch((error) => {
      console.error(error);
      res.status(500);
    });
};

const optimisticUpdateUserInfo = (models) => (req, res) => {
  const { users } = models;
  let { email, ...rest } = req.body;

  users
    .findOne({ where: { usr_email: req.user.user.usr_email } })
    .then((userExist) => {
      if (!userExist) {
        throw new Error(`User is not found`);
      }
      if (userExist) {
        return userExist
          .update(rest)
          .then((updated) => res.json({ message: "success" }));
      }
    })
    .catch((error) => console.error(error));
};

const RegisterOTP = (models) => (req, res) => {
  const { users } = models;
  let { email } = req.body;
  console.log(email);
  users
    .findOne({ where: { usr_email: email } })
    .then((userExist) => {
      if (!userExist) {
        throw new Error(`User is not found`);
      }
      if (userExist && userExist.usr_otp_secret.length > 0) {
        return res.json({ secret: userExist.usr_otp_secret });
      }

      const temp_secret = speakeasy.generateSecret();
      return userExist
        .update({ usr_otp_secret: temp_secret.base32 })
        .then((_) => {
          res.json({ secret: userExist.usr_otp_secret });
        });
    })
    .catch((error) => console.error(error));
};

const verifyOTP = (models) => (req, res) => {
  const { email, token } = req.body;

  models.users
    .findOne({ where: { usr_email: email } })
    .then((userExist) => {
      if (!userExist) {
        throw new Error(`User is not found`);
      }

      const tokenValidates = speakeasy.totp.verify({
        secret: userExist.usr_otp_secret,
        encoding: "base32",
        token,
        window: 1,
      });
      if (tokenValidates) {
        userExist
          .update({ is_otp_active: 1 })
          .then((_) => res.json({ validated: true }));
      } else {
        res.json({ validated: false });
      }
    })
    .catch((error) => console.error(error));
};

module.exports = {
  verifyUser,
  registerUser,
  optimisticUpdateUserInfo,
  RegisterOTP,
  verifyOTP,
};
