const getAllCategories = (models) => (req, res) => {
  const { product_category } = models;

  product_category
    .findAll({
      include: [
        {
          model: models.sub_category,
          as: "sub_categories",
        },
      ],
    })
    .then((result) => res.json({ categories: result }))
    .catch((err) => console.error(err));
};

module.exports = { getAllCategories };
