const User = require("./User");
const Category = require("./Category");
const Products = require("./Products");
const Stripe = require("./Stripe");
const Orderdetail = require("./Orderdetail");

module.exports = { User, Category, Products, Stripe, Orderdetail };
