const getOrders = (models) => (req, res) => {
  const { order_detail } = models;
  const { userID } = req.params;

  order_detail
    .findAll({
      where: { user_id: userID },
      include: [
        {
          model: models.order_product_detail,
          include: [
            {
              model: models.products,
              include: [
                {
                  model: models.product_images,
                },
              ],
            },
          ],
        },
      ],
    })
    .then((data) => res.json(data))
    .catch((error) => console.error(error));
};

module.exports = { getOrders };
