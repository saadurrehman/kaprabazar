const getAllProducts = (models) => (req, res) => {
  const { products } = models;

  products
    .findAll({
      include: [
        {
          model: models.product_images,
        },
      ],
    })
    .then((result) => res.json({ products: result }))
    .catch((err) => console.error(err));
};

const getProductsByCategoryID = (models) => (req, res) => {
  const { products } = models;
  const { categoryID, subCategoryID } = req.params;

  products
    .findAll({
      where: {
        "product_category_id": categoryID,
        product_subcategory_id: subCategoryID,
      },
      include: [
        {
          model: models.product_images,
        },
      ],
    })
    .then((result) => res.json({ products: result }))
    .catch((err) => console.error(err));
};

module.exports = { getAllProducts, getProductsByCategoryID };
