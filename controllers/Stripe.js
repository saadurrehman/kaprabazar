const stripe = require("stripe")(process.env.STRIPE_SECRET);
const connection = require("../connection");

const checkoutWithPaymentMethod = (models) => async (req, res) => {
  const transaction = await connection.transaction();
  const { order_detail, order_product_detail } = models;
  console.log(req.body);
  const { id, products, ...restBody } = req.body;
  try {
    const payment = await stripe.paymentIntents.create({
      amount: 5000,
      currency: "USD",
      description: "Your Company Description",
      payment_method_types: ["card"],
      payment_method: "pm_card_visa",
      // payment_method: id,
      confirm: true,
    });
    console.log("stripe-routes.js 19 | payment", payment);
    const { dataValues: orderCreated } = await order_detail.create(
      { stripe_payment_id: payment.id, ...restBody },
      {
        transaction,
      }
    );

    const { order_id } = orderCreated;

    const productObjectArray = products.map(
      ({ product_id, cartProductQuantity }) => ({
        order_id,
        product_id,
        product_quantity: cartProductQuantity,
      })
    );

    await order_product_detail.bulkCreate(productObjectArray, {
      transaction,
    });

    transaction.commit();
    res.json({
      message: "Payment Successful",
      success: true,
    });
  } catch (error) {
    console.error(error);
    await transaction.rollback();
  }
};

const checkoutWithCashOnDelivery = (models) => async (req, res) => {
  const transaction = await connection.transaction();
  const { order_detail, order_product_detail } = models;
  const { products, ...restBody } = req.body;

  try {
    const { dataValues: orderCreated } = await order_detail.create(restBody, {
      transaction,
    });

    const { order_id } = orderCreated;

    const productObjectArray = products.map(
      ({ product_id, cartProductQuantity }) => ({
        order_id,
        product_id,
        product_quantity: cartProductQuantity,
      })
    );

    await order_product_detail.bulkCreate(productObjectArray, {
      transaction,
    });

    transaction.commit();
    res.json({
      message: "Order complete",
      success: true,
    });
  } catch (error) {
    console.error(error);
    await transaction.rollback();
  }
};

module.exports = { checkoutWithPaymentMethod, checkoutWithCashOnDelivery };
